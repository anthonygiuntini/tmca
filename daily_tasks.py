# This file should be run daily (it increases the speed for the end users by reducing processes each time they load it)

# This is required before using django features
import os
import django
#os.environ['DJANGO_SETTINGS_MODULE'] = 'Wham.dev_settings'
os.environ['DJANGO_SETTINGS_MODULE'] = 'Wham.settings'

django.setup()


import Checkup

from meal_count.tools import *
from meal_count.models import Person, Day
from Wham.parameters import *
from django.core.management import call_command
from django.utils import timezone
from django.core.mail import EmailMessage



# if not settings.configured:
#     settings.configure(settings, DEBUG=True)

def backup_db(suffix=''):
    """
    Dumps the data from the database into the directory specified in the parameters file.
    :suffix: appended right before backup file extension
    :return:
    """
    # Check if directory exists, if not create it
    if not os.path.exists(DB_BACKUP_DIR):
        os.mkdir(DB_BACKUP_DIR)
    # Set backup path, then call the dumpdata command
    backup_path = os.path.join(DB_BACKUP_DIR, timezone.datetime.now().strftime("%Y-%m-%d")+'_backup'+suffix+'.json')
    output = open(backup_path, 'w')  # Point stdout at a file for dumping data to.
    call_command('dumpdata', format='json', indent=3, stdout=output)
    output.close()

    # Send email of backup
    try:
        msg = EmailMessage('TMCADB Backup: '+timezone.datetime.now().strftime("%Y-%m-%d"), 'See attached',
                           'themealcountapp@gmail.com', ['themealcountapp@gmail.com'])
        msg.content_subtype = "html"
        msg.attach_file(backup_path)
        msg.send()
    except:
        print("Attempt to email backup failed")
        ROUTINE_LOGGER.error('Attempt to email backup failed')


def run():
    """
    Container function
    :return:
    """
    print('=====Daily task started=====')
    print('Backing up database...')
    ROUTINE_LOGGER.info('Daily Task Started')
    ROUTINE_LOGGER.info('Backing up database...')
    backup_db()

    print('Generating dates...')
    ROUTINE_LOGGER.info('Generating dates...')
    generate_dates()

    print('Disabling meals...')
    ROUTINE_LOGGER.info('Disabling meals...')
    disable_meals() # this may have to be run more than daily

    print('Deleting expired Guests...')
    ROUTINE_LOGGER.info('Deleting expired Guests...')
    delete_expired_guests()

    print('Populating statuses...')
    ROUTINE_LOGGER.info('Populating statuses...')
    days = Day.objects.filter(date__gte=timezone.now().date())
    people = Person.objects.filter(active=True)
    auto_fill_status(people, days)

    print('Running checkup...')
    ROUTINE_LOGGER.info('Running checkup...')
    Checkup.Checkup().comprehensive_checkup()

    print('====Daily tasks complete====')
    ROUTINE_LOGGER.info('====Daily tasks complete====')


if __name__ == "__main__":
    run()

