# Script to keep track of overall stats and health


# This is required before using django features
import os
import django
#os.environ['DJANGO_SETTINGS_MODULE'] = 'Wham.dev_settings'
os.environ['DJANGO_SETTINGS_MODULE'] = 'Wham.settings'

django.setup()

import meal_count.tools as tools
import uuid

from meal_count.models import Center, Person, Day, Status
from Wham.parameters import *
from django.core.mail import send_mail
from django.utils import timezone

class Checkup:

    def __init__(self, email=True):
        """
        Class to examine database. Class structure makes it more organized.
        email: if True send summary email even if there is no error.
        """
        self.centers = Center.objects.all()
        self.people = Person.objects.all()
        self.days = Day.objects.all()
        self.statuses = Status.objects.all()

        self.id = str(uuid.uuid4()) # ID to easily id log info
        self.email = email
        CHECKUP_LOGGER.info("+"+self.id+"+")
        print("+"+self.id+"+")

        self.report = ''

    def comprehensive_checkup(self):
        """
        Runs full battery of tests and stats.
        :return:
        """
        CHECKUP_LOGGER.info("==========CHECKUP STARTED==========")
        print("==========CHECKUP STARTED==========")
        self.report += "==========CHECKUP STARTED==========\n"

        CHECKUP_LOGGER.info("---General Stats---")
        print("---General Stats---")
        self.report += "---General Stats---\n"
        CHECKUP_LOGGER.info("# Centers: " + str(self.centers.count()))
        print("# Centers: " + str(self.centers.count()))
        self.report += "# Centers: " + str(self.centers.count()) + "\n"
        CHECKUP_LOGGER.info("# People: " + str(self.people.count()))
        print("# People: " + str(self.people.count()))
        self.report += "# People: " + str(self.people.count()) + "\n"
        CHECKUP_LOGGER.info("# Active People: " + str(self.people.filter(active=True).count()))
        print("# Active People: " + str(self.people.filter(active=True).count()))
        self.report += "# Active People: " + str(self.people.filter(active=True).count()) + "\n"

        date_res = self.check_dates()
        guest_res = self.check_guests()

        total_res = date_res + guest_res

        # message = ''
        # with open(os.path.join(LOG_DIR, 'checkup_log.txt')) as file:
        #     contents = file.read()
        #     try:
        #         message = contents.split(self.id, 1)[1]
        #     except IndexError:
        #         message = "Failed to pull from log or log is empty."

        if False in total_res:
            send_mail(
                'TMCA Checkup - FAILED',
                self.report,
                'themealcountapp@gmail.com',
                ['themealcountapp@gmail.com', 'giuntini1996@gmail.com'],
                fail_silently=False,
            )
        else:
            if self.email:
                send_mail(
                    'TMCA Checkup',
                    self.report,
                    'themealcountapp@gmail.com',
                    ['themealcountapp@gmail.com'],
                    fail_silently=False,
                )

        CHECKUP_LOGGER.info("==========CHECKUP ENDED==========")
        print("==========CHECKUP ENDED==========")

    def check_dates(self):
        """
        Makes sure dates are being generated.
        :return:
        """
        results = []
        for center in self.centers:
            # Get days greater than or equal to today for each center
            days = self.days.filter(center=center, date__gte=tools.current_localtime(center.timezone).date())

            day_count = days.count()
            latest_date = days.latest('date')
            earliest_date = days.earliest('date')

            CHECKUP_LOGGER.info("---Date Checker for "+center.name+" ---")
            print("---Date Checker for "+center.name+" ---")
            self.report += "---Date Checker for "+center.name+" ---" + '\n'
            CHECKUP_LOGGER.info("Days ahead: " + str(day_count))
            print("Days ahead: " + str(day_count))
            self.report += "Days ahead: " + str(day_count) + '\n'
            CHECKUP_LOGGER.info("Earliest Date: " + str(earliest_date))
            print("Earliest Date: " + str(earliest_date))
            self.report += "Earliest Date: " + str(earliest_date) + '\n'
            CHECKUP_LOGGER.info("Latest Date: " + str(latest_date))
            print("Latest Date: " + str(latest_date))
            self.report += "Latest Date: " + str(latest_date) + '\n'
            if day_count >= 30:
                results.append(True)
            else:
                results.append(False)
                CHECKUP_LOGGER.error("Date Checker for " + center.name + " FAILED")
                print("---Date Checker for " + center.name + " FAILED")
                self.report += "---Date Checker for " + center.name + " FAILED" + '\n'

        return results

    def check_guests(self):
        """
        Checks to ensure that guests are being removed.
        :return:
        """
        results = []
        for center in self.centers:

            CHECKUP_LOGGER.info("---Guest Checker for "+center.name+" ---")
            print("---Guest Checker for "+center.name+" ---")
            self.report += "---Guest Checker for "+center.name+" ---" + '\n'
            guests = self.people.filter(center=center, guest=True)

            guest_count = guests.count()
            CHECKUP_LOGGER.info("# Guests: " + str(guest_count))
            print("# Guests: " + str(guest_count))
            self.report += "# Guests: " + str(guest_count) + '\n'

            subres = []
            for guest in guests:
                guest_statuses = self.statuses.filter(person=guest)
                # if the latest status is earlier than today's date, test fails
                if guest_statuses.latest('meal__day__date').meal.day.date < tools.current_localtime(center.timezone).date():
                    subres.append(False)
                else:
                    subres.append(True)
            if False in subres:
                results.append(False)
                CHECKUP_LOGGER.error("Guest Checker for " + center.name + " FAILED")
                print("---Guest Checker for " + center.name + " FAILED")
                self.report += "---Guest Checker for " + center.name + " FAILED" + '\n'
            else:
                results.append(True)

        return results


if __name__ == '__main__':

    checkup = Checkup(email=False)
    checkup.comprehensive_checkup()


