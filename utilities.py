# This is required before using django features
import os
import django

environment = input('Which environment? (dev or deploy):\n')

if environment == 'dev':
    os.environ['DJANGO_SETTINGS_MODULE'] = 'Wham.settings'
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'Wham.deploy_settings'

django.setup()

from meal_count.tools import *
from meal_count.models import Person, Day, Meal
from Wham.parameters import *
from django.core.management import call_command
from django.utils import timezone


def update_meal_centers():
    """
    Checks to see if any meal instances do not have a center. If so, it takes the center from the mealoption center.
    :return:
    """
    all_meals = Meal.objects.all()
    count = 0
    for meal in all_meals:
        if meal.center is None:
            meal.center = meal.day.center
            meal.save()
            count += 1
    print('Updated '+str(count)+' Meals')


if __name__ == '__main__':

    command = input('Enter option: \n1-fill in centers for all meals\n')

    if command == '1':
        update_meal_centers()

# if not settings.configured:
#     settings.configure(settings, DEBUG=True)