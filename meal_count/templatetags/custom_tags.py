import json

from django import template
from django.core.exceptions import ObjectDoesNotExist

from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict

from django.template import Library

from meal_count.models import Meal
register = template.Library()

@register.simple_tag
def define(val=None):
  return val

@register.filter(name='by_person')
def by_person(set, person):
    """Returns a meal set that contains all of the status in set that are linked to the person. Or it returns
    None if nothing was found. This happens in the case of a guest."""

    try:
        new_set = set.get(person=person)
    except ObjectDoesNotExist:
        new_set = None

    return new_set

@register.filter(name='by_status')
def by_status(set, status_option):
    """
    Returns the statuses from "set" that have the status_option.
    set: must be a qs of statii
    """
    try:
        new_set = set.filter(status=status_option)
    except ObjectDoesNotExist:
        new_set = None

    return new_set

@register.filter(name='by_meal')
def by_meal(set, meal_name):
    """
    Returns the statuses from "set" that have the meal name "meal_name".
    set: must be a qs of statii
    """
    try:
        new_set = set.filter(meal__meal_type__name=meal_name)
    except ObjectDoesNotExist:
        new_set = None

    return new_set


@register.filter(name='sort_by')
def sort_by(queryset, element_name):
    """
    Returns the statuses from "set" that have the meal name "meal_name".
    set: must be a qs of statii
    """
    sorted = queryset.order_by(element_name)

    return sorted

@register.filter(name='remove_item')
def remove_first(list1, index):
    """This removes the item at the index from list1 and returns the new list"""
    del list1[index]
    return list1

@register.filter(name='dict_val')
def dict_val(dictionary, key):
    """This removes the item at the index from list1 and returns the new list"""
    try:
        return dictionary[key]
    except KeyError:
        return None

@register.filter(name='get_item')
def get_at_index(list1, index):
    """This returns the item from list1 at the given index"""
    return list1[index]

@register.filter(name='is_zero')
def empty_option(count_dict):
    """This returns the item from list1 at the given index"""
    if max(count_dict.values()) == 0:
        return True
    else:
        return False

@register.filter( is_safe=True )
def jsonify(jobject):
    """
    Converts jobject into json format. Taken from: https://stackoverflow.com/questions/4698220/django-template-convert-a-python-list-into-a-javascript-object
    :param jobject:
    :return:
    """

    if isinstance(jobject, QuerySet):
        return serialize('json', jobject)
    return json.dumps(jobject)