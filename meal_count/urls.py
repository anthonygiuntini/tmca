#URL config for meal_count app

from django.conf.urls import url
from django.contrib.auth import views as authviews
from django.views.generic.base import RedirectView
from django.contrib.auth.views import LogoutView
from django.utils.translation import ugettext_lazy as _

from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

app_name = 'meal_count'

auth_urlpatterns = ([
    path(_('login/'), authviews.LoginView.as_view(), name='auth_login'),
    path(_('logout/'), authviews.LogoutView.as_view(), name='auth_logout'),

    path(_('password_change/'), authviews.PasswordChangeView.as_view(), name='password_change'),
    path(_('password_change/done/'), authviews.PasswordChangeDoneView.as_view(), name='password_change_done'),

    path(_('password_reset/'), authviews.PasswordResetView.as_view(), name='password_reset'),
    path(_('password_reset/done/'), authviews.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path(_('reset/<uidb64>/<token>/'), authviews.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path(_('reset/done/'), authviews.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
], 'auth')

urlpatterns = [
    path('', views.index, name='index'),
    path(_('default'), views.index_default, name='index_default'),
    path(_('guests'), views.guests, name='guests'),
    path(_('guests/delete'), views.delete_guest, name='delete_guest'),
    path(_('view/<str:week_delta>/'), views.index, name='altered_index'),
    path(_('all/'), views.view_all, name='view_all'),
    path(_('all/defaults'), views.view_all_defaults, name='view_all_defaults'),
    path(_('all/<str:week_delta>/'), views.view_all, name='view_all'),
    path('generate', views.generate_dates, name='generate'),
    path(_('update_status'), views.update_status, name='update_status'),
    url(r'^verify/$', views.verify_attendance, name='verify_attendance'),
    path(_('verify/<str:current_meal>/'), views.verify_attendance, name='verify_attendance'),
    path(_('verify/set_act_status'), views.set_act_status, name='set_act_status'),
    path(_('manage'), views.manage, name='manage'),
    path(_('manage/meals'), views.meals, name='manage_meals'),
    path(_('manage/meals/<str:week_delta>/'), views.meals, name='manage_meals'),
    path(_('manage/users'), views.manage_users, name='manage_users'),
    path(_('manage/users/<str:username>'), views.manage_users, name='manage_users'),
    path(_('manage/add_opt'), views.add_option, name='add_statusoption'),
    path(_('manage/del_opt'), views.delete_option, name='delete_statusoption'),
    path(_('manage/del_diet'), views.delete_diet, name='delete_diet'),
    path(_('manage/add_diet'), views.add_diet, name='add_diet'),
    path(_('manage/del_meal'), views.delete_meal, name='delete_meal'),
    path(_('manage/add_meal'), views.add_meal, name='add_meal'),
    path(_('manage/set_override'), views.set_override, name='set_override'),
    path(_('manage/add_message'), views.add_message, name='add_message'),
    path(_('manage/set_email'), views.set_email, name='set_email'),
    path(_('manage/toggle_status'), views.toggle_status_option, name='toggle_status'),
    path(_('manage/change_person_status'), views.change_person_status, name='change_person_status'),
    path(_('<str:center>/report'), views.count_report, name='current_count'),
    path(_('<str:center>/report/week'), lambda request, center: views.count_report(request, center=center, report_type='week'), name='current_count_week'),
    #path(_('<str:center>/classic'), views.classic_report, name='classic_report'),
    path(_('<str:center>/initials'), lambda request, center: views.count_report(request, center=center, is_initials=True), name='initials_report'),
    path(_('<str:center>/initials/week'), lambda request, center: views.count_report(request, center=center, report_type='week', is_initials=True), name='initials_report_week'),
    path('init-meal-count', views.initialize_meal_count, name='initialize_app'),
    path(_('<str:center>/create_account'), views.create_user, name='create_user'),
    path(_('logout'), views.logout_user, name='logout_user'),
    #path('accounts/login/', views.custom_login, name='custom_login'),
    url(_('accounts/profile'), RedirectView.as_view(url='/', permanent=False)),
    path(_('accounts/'), include(auth_urlpatterns)),#'django.contrib.auth.urls')),
]

