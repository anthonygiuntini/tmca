$(document).ready(function () {
    // $('count_table').floatThead({
    //     position: 'absolute'
    // });
    $('.status-cell.clickable').click(function () {
        //Get the statusoption id and the current status abbr
        let id = $(this).attr('id');
        let status_val = $(this).text();

        console.log('clicked' + id + ' - ' + status_val);
        //send to the server
        $.post($('#update-post-url').text(),
            {
                status_id: id,
                status_val: status_val,
                //Need csrfmiddleware token to avoid errors
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            function (data) {
                console.log('POST attempt returned: ' + data);
                if (data[0] === 'y') {
                    //if first char is y, input matches
                    console.log('Status ID #' + id);  //then get the rest of the string (excluding "s")
                    $('#' + id).text(data.substring(1)); //change status text within the clicked status box
                    $('#' + id).css("background-color", "#fff");
                    $('#' + id).css("color", "inherit");
                } else if (data[0] == 'n') {
                    //if first char is n, input does not match (change to red)
                    console.log('Status ID #' + id);  //then get the rest of the string (excluding "s")
                    $('#' + id).text(data.substring(1)); //change status text within the clicked status box
                    $('#' + id).css("background-color", "#9F0700");
                    $('#' + id).css("color", "white");
                } else {
                        console.log('error')
                }

            });
    });
});