$(document).ready(function () {
    $("body").tooltip({selector: '[data-toggle=tooltip]'});

    $('.status-box:not(.disabled)').click(function () {
        //Turn on status spinner
        $('#status-spinner').show();

        //Get the statusoption id and the current status abbr
        let id = $('.status-text', this).attr('id');
        let status_val = $('.status-text', this).text();
        let s_id = $(this).attr('data-s-class');
        // Check tools pallet for quick select value. If its different from default, set status_val to that
        let quick_sel_val = $('.tools-option.active').text();
        if (quick_sel_val != '-') {
            // prepend qs_ to the status_val to indicate to the server that its a quick select val
            status_val = 'qs_' + quick_sel_val;
        }

        console.log('clicked' + id + ' - ' + status_val);
        //send to the server
        $.post($(this).attr('data-url'),
            {
                status_id: id,
                status_val: status_val,
                //Need csrfmiddleware token to avoid errors
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            function (data) {
                console.log('POST attempt returned: ' + data);
                if (data[0] === 's') {
                    //First char must be "s" for success
                    console.log('Status ID #' + id);  //then get the rest of the string (excluding "s")

                    $('#' + id).text(data.substring(1)); //change status text within the clicked status box

                    //hide status spinner
                    $('#status-spinner').hide();

                    //Show the success badge quickly stop any previous animation
                    $('.alert-success').stop(true, true).fadeIn();
                    $('.alert-success').fadeOut();
                    $('#' + id).removeClass('text-warning');
                    $('#' + id).addClass('text-white');
                    $('#' + id + '.update-success').show();
                    /*$('#' + id + '.update-success').fadeIn("fast", function(){
                        $('#' + id + '.update-success').fadeOut("fast");
                    });*/
//                    $('#' + id).css("color", "green");
                } else {
                    console.log('error')
                }
            });
    });
    $('.add-message').click(function () {
        let day_id = $(this).attr('id');


    });


    //Make tables the same width
    let widest = 0;
    $(".count_table").each(function () {
        //Get largest element's width
        if ($(this).width() > widest) {
            widest = $(this).width();
        }
    });
    //Then set each title cell (which defines the table width) to that width
    $(".ct-title-cell").each(function () {
        $(this).css("width", widest);
    });

    //Make message cells the same height
    let tallest = 0;
    $(".msg-cell").each(function () {
        //Get largest element's height
        if ($(this).height() > tallest) {
            tallest = $(this).height();
        }
    });
    //Then set each msg cell to that height
    $(".msg-cell").each(function () {
        $(this).css("height", tallest);
    });

    $('.tools-indicator').click(function () {
        //Remove the active class from the previous option
        //Add the active class to the new button
        $('.tools-selector-group').toggleClass('d-none');
    });

    $('.tools-option').click(function () {
        //Remove the active class from the previous option
        //Add the active class to the new button
        $('.tools-option.active').removeClass('active');
        $(this).addClass('active');
        $('.tools-indicator').text($(this).text());
        $('.tools-selector-group').addClass('d-none');
    });

    $('#show-initials').click(function () {
        $('.initials-list-item').toggle();
    });

});

function set_override(sel, mc_clicked) {
    //when a select element changes
    // if (this.hasClass('override-sel')) {
    if (sel.classList.contains('override-sel')) {
        //Make sure it is an override select
        let meal_id = sel.id;

        let override_val = sel.value;
        let manual_count = 0;
        let entry = $("#me" + meal_id);
        let send_request = true;
        //send to the server
        if (sel.value == "Manual Count" && !mc_clicked) {
            //    If the select value specifies a manual count, but the mc_set button has not been clicked show the manual count window
            $('#manual-count-modal-' + meal_id).modal('show');
            send_request = false;
        } else if (sel.value == "Manual Count" && mc_clicked) {
            // Otherwise if its selected and the mc_set button is clicked
            override_val = "Manual Count";
            manual_count = $("#mi" + meal_id).val();
            console.log(manual_count);
        } else {
            if (sel.value == "All Packed") {
            }
            // Hide the manual count box if it was shown
            if (entry.css('display') != 'none') {

                entry.hide();
            }
        }
        if (send_request) {

            $.post($(sel).attr('data-url'),
                {
                    meal_id: meal_id,
                    override_val: override_val,
                    manual_count: manual_count, // No need for manual count here
                    //Need csrfmiddleware token to avoid errors
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                function (data) {
                    console.log('POST attempt returned: ' + data);
                    if (data === 'success') {
                        //the data returned must be success
                        console.log('override for meal_id #' + meal_id + ' has been set');
                        //Reload for success confirmation
                        location.reload();
                    } else {
                        console.log('error');
                        sel.value = "No Override";
                    }
                });
        } else {

        }
    }

}

function post_override(meal_id, override_val, manual_count, csrf) {

}

function set_message(btn) {
    let day_id = $(btn).attr('data-d-id');
    //concat #i and the id to get the id of the input row
    let input_row = $('#i' + day_id);
    let input_state = input_row.css('display');
    let msg = $('#in' + day_id).val();
    let msg_type = $(btn).attr('data-msg-type');
    console.log(msg);
    if (msg_type == null){
        msg_type = 'day';
    }
    //submit the data
    $.post($(btn).attr('data-url'),
        {
            day_id: day_id,
            message: msg,
            msg_type: msg_type,
            //Need csrfmiddleware token to avoid errors
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        function (data) {
            console.log('POST attempt returned: ' + data);
            if (data === 'success') {
                location.reload();
            } else {
                console.log('error')
            }
        });
}