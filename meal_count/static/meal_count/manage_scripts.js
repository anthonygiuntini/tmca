$( document ).ready(function() {
    // $('options_table').floatThead({
    //     position: 'absolute'
    // });

    $('#submit-add-form').click(function() {
        //Adds a new status option
        console.log('Adding new status option');
        $.post( "/manage/add_opt",
            {
                opt_name: $('input[name=opt_name]').val(),
                opt_code: $('input[name=opt_code]').val(),
                opt_preptime: $('input[name=opt_preptime]').val(),
                //Need csrfmiddleware token to avoid errors
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            function (data) {
                console.log('POST attempt returned: ' + data);
                if (data==='success'){
                    window.location.replace('/manage');
                } else {
                    alert(data);
                }

            });
    });

    $('.del-opt').click(function() {
        //Deletes the specified status option
        if (confirm('Deleting this will delete all recorded options. Are you sure you want to do that?')) {
            let id = $(this).attr('id');
            console.log('Deleting #' + id);
            $.post("/manage/del_opt",
                {
                    opt_id: id,
                    //Need csrfmiddleware token to avoid errors
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                function (data) {
                    console.log('POST attempt returned: ' + data);
                    window.location.replace('/manage');
                });
        }
    });

     //Change status box text upon click
    $('#add-diet').click(function() {
        $('#new-diet').show();

    });

    $('#submit-add-diet-form').click(function() {
        // $('#add-diet-form').submit();

        console.log('Adding new diet');
        $.post( "/manage/add_diet",
            {
                diet_name: $('input[name=diet_name]').val(),
                diet_code: $('input[name=diet_code]').val(),
                diet_description: $('input[name=diet_description]').val(),
                //Need csrfmiddleware token to avoid errors
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            function (data) {
                console.log('POST attempt returned: ' + data);
                if (data==='success'){
                    window.location.replace('/manage');
                } else {
                    alert(data);
                }

            });
    });

    $('.del-diet').click(function() {
        //Deletes the specified status option
        if (confirm('Deleting this will delete all connected statuses. Are you sure you want to do that?')) {
            let id = $(this).attr('id');
            console.log('Deleting #' + id);
            $.post("/manage/del_diet",
                {
                    diet_id: id,
                    //Need csrfmiddleware token to avoid errors
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                function (data) {
                    console.log('POST attempt returned: ' + data);
                    window.location.replace('/manage');
                });
        }
    });

        //Change status box text upon click
    $('#add-meal').click(function() {
        $('#new-meal').show();

    });

    $('#submit-meal-form').click(function() {
        // $('#add-diet-form').submit();

        console.log('Adding new meal option');
        $.post( "/manage/add_meal",
            {
                meal_name: $('input[name=meal_name]').val(),
                meal_code: $('input[name=meal_code]').val(),
                meal_cutoff: $('input[name=meal_cutoff]').val(),
                meal_time: $('input[name=meal_time]').val(),
                meal_cutoff_day: $('input[name=meal_cutoff_day]').val(),
                //Need csrfmiddleware token to avoid errors
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            function (data) {
                console.log('POST attempt returned: ' + data);
                if (data === 'success'){
                    window.location.replace('/manage');
                } else {
                    alert(data);
                }

            });
    });

    $('.del-meal').click(function() {
        //Deletes the specified status option
        if (confirm('Deleting this will delete all connected statuses. Are you sure you want to do that?')) {
            let id = $(this).attr('id');
            console.log('Deleting #' + id);
            $.post("/manage/del_meal",
                {
                    meal_id: id,
                    //Need csrfmiddleware token to avoid errors
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                function (data) {
                    console.log('POST attempt returned: ' + data);
                    window.location.replace('/manage');
                });
        }
    });

    $('.status-active-cb').click(function(){
        let id =  $(this).attr('id');
        console.log('Changing active state of #' + id);
        $.post("/manage/toggle_status",
            {
                status_id: id,
                //Need csrfmiddleware token to avoid errors
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            function (data) {
                console.log('POST attempt returned: ' + data);
                window.location.replace('/manage');
            });
    });

    $(window).scroll(function () {
       let y = $(this).scrollTop();
       if (y >= 200){
           $('.submenu').addClass('fixed-sub');
       }else{
           $('.submenu').removeClass('fixed-sub');
       }
    });

    //On manage suser page - change person status
    $('#user-status-input').change(function() {
        //Adds a new status option
        console.log('Toggling person status');
        $.post( "/manage/change_person_status",
            {
                person_id: $('input[name=person-id]').val(),
                person_status: $('#user-status-input').is(':checked'),
                //Need csrfmiddleware token to avoid errors
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            function (data) {
                console.log('POST attempt returned: ' + data);
                if (data==='success'){
                    console.log('Successful change')
                } else {
                    alert(data);
                }

            });
    });

});




