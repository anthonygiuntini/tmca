$(document).ready(function () {
   $('#add-guest').click(function () {
      $('#new-guest-container').show()
   });

   $('.delete-link').click(function() {
       //Show confirmation modal
       $('#confirm-modal').modal();
        //add fuest id to delete-data-id value
       $('#delete-confirm').attr('data-delete-id',$(this).attr('id'));

    });
    $('#delete-confirm').click(function() {
            let id = $('#delete-confirm').attr('data-delete-id');
            console.log('Deleting #' + id);
            $.post($('#delete-confirm').attr('data-url'),
                {
                    guest_id: id,
                    //Need csrfmiddleware token to avoid errors
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                function (data) {
                    console.log('POST attempt returned: ' + data);
                    window.location.replace($('#delete-confirm').attr('data-url2'));
                });
    });
});


