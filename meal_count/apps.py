from django.apps import AppConfig

class MealCountConfig(AppConfig):
    name = 'meal_count'
