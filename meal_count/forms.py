#Forms.py: custom forms

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from .models import Person, Meal, Diet

class CreateUserForm1(UserCreationForm):
    email = forms.EmailField(label="Email")

    class Meta:
        model = User
        fields = ("email", "username",)

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("Email already exists")
        return email

    def save(self, commit=True):
        user = super(CreateUserForm1, self).save(commit=False)

        #user.username = username

        user.email = self.cleaned_data["email"]

        if commit:
            user.save()
        return user

    def __init__(self, *args, center_i=None, **kwargs):
        # override init to filter diets to only the current center
        super(CreateUserForm1, self).__init__(*args, **kwargs)
        # Add form control class to inputs
        for visible in self.visible_fields():

            visible.field.widget.attrs['class'] = 'form-control'


class CreateUserForm2(forms.ModelForm):
    class Meta:
        """Makes the from for the create user model. This is the second part (the extended Person model)"""
        model = Person
        exclude = ['created_at', 'user', 'meals', 'guest', 'active', 'center']

    def __init__(self, *args, center_i=None, **kwargs):
        # override init to filter diets to only the current center
        super(CreateUserForm2, self).__init__(*args, **kwargs)
        # Add form control class to inputs
        for visible in self.visible_fields():
            if visible.field.label == 'Diet':
                visible.field.widget.attrs['class'] = 'custom-select'
            else:
                visible.field.widget.attrs['class'] = 'form-control'


        if center_i:
            self.center = center_i
            self.fields['diet'].queryset = Diet.objects.filter(center=center_i)



class NewGuestForm(forms.ModelForm):

    class Meta:
        """Form for a guest. Which is a special instance of the Person model"""
        model = Person
        exclude = ['created_at', 'center', 'guest', 'user']

    def __init__(self, *args, **kwargs):
        """Overwrite __init__ to filter meal option values"""

        # this is required to avoid a user kwarg supplied twice error
        # NOTE: this has to be done for any kwarg
        user = kwargs.pop('user')

        super(NewGuestForm, self).__init__(*args, **kwargs)
        #Add form control class to inputs
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

        if user:
            center = user.person.center
            now = timezone.now()
            meal_qs = Meal.objects.filter(day__center=center, day__date__gte=now)
            self.fields['meals'].queryset = meal_qs.order_by('day__date', 'meal_type__meal_time')

            diet_qs = Diet.objects.filter(center=center)
            self.fields['diet'].queryset = diet_qs
            self.fields['diet'].to_field_name = 'name'
