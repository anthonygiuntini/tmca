from datetime import time

from Wham.parameters import *
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

def default_cutoff_time():
    """Function that returns the default cutoff time (8am)"""
    return time(hour=8)


class Center(models.Model):
    '''Model that contains all data for a center.'''
    name = models.CharField(max_length=20, default='Schuyler')
    directors_email = models.EmailField(default='giuntini1996@gmail.com')
    created_at = models.DateTimeField(auto_now_add=True)
    timezone = models.CharField(default=DEFAULT_TZ, max_length=40)
    #lang_code = models.CharField(default='en', max_length=10)

    # Center's settings
    show_count_in_initials = models.BooleanField(default=True, null=True)

    def __str__(self):
        """Make useful str name"""
        return self.name

    def save(self, *args, **kwargs):
        """Overwrite save function to generate default StatusOption for the new user"""

        if not self.pk:
            # if no id, this is a new instance
            super().save(*args, **kwargs)  # must put this here so that person is saved before itis used

            # Create these defaults on creation of the center
            StatusOption(name=_('Absent'), abbreviation=_('O'), priority=0, prep_time=0, center=self, can_edit=False, active=True).save()
            StatusOption(name=_('Present'), abbreviation=_('X'), priority=3, center=self, can_edit=False, active=True).save()
            StatusOption(name=_('Packed'), abbreviation=_('P'), priority=4, center=self, can_edit=False, active=True).save()
            StatusOption(name=_('Late'), abbreviation=_('L'), priority=5, center=self, can_edit=False, active=True).save()
            StatusOption(name=_('Early'), abbreviation=_('E'), priority=6, center=self, can_edit=False, active=True).save()
            StatusOption(name=_('Early Packed'), abbreviation=_('EP'), priority=7, center=self, can_edit=False, active=True).save()
            StatusOption(name=_('Packed before Breakfast'), abbreviation=_('PPB'), priority=1, center=self, can_edit=False,
                         active=True, calculated=True).save()
            StatusOption(name='Packed at Breakfast', abbreviation=_('PAB'), priority=2, center=self, can_edit=False,
                         active=True, calculated=True).save()
            Diet(center=self, name=_('None'), code='N').save()
            MealOption(name=_('Breakfast'), code='B', center=self,
                       cutoff_time=time(hour=8), cutoff_day=1, meal_time=time(hour=8), can_edit=False).save()
            MealOption(name=_('Lunch'), code='L', center=self,
                       cutoff_time=time(hour=8), meal_time=time(hour=12), can_edit=False).save()
            MealOption(name=_('Dinner'), code='D', center=self,
                       cutoff_time=time(hour=12), meal_time=time(hour=19), can_edit=False).save()

        super().save(*args, **kwargs)


class Diet(models.Model):
    """Model for a diet. Contains diet code and description."""
    name = models.CharField(max_length=16, default='None')
    code = models.CharField(max_length=6, default='N')  # lettered/numbered code for diet
    description = models.CharField(max_length=60, default='No diet', blank=True)
    can_edit = models.BooleanField(default=True)
    priority = models.IntegerField(default=100, blank=True)
    center = models.ForeignKey(Center, on_delete=models.CASCADE)

    def __str__(self):
        """Make useful str name"""
        return self.name #+ ' - ' + self.center.name


class Day(models.Model):
    '''Model for a single day. Contains date, day of the week, and occasion'''
    date = models.DateField()
    message = models.CharField(default=None, null=True, max_length=40, blank=True)  # message to be displayed to users

    center = models.ForeignKey(Center, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        """Overwrite save function to generate statuses for the new user"""

        if self.pk is None:
            # if there is no id, the instance is new
            super().save(*args, **kwargs)  # must put this here so that person is saved before itis used

            # generate new Meal instances
            for option in MealOption.objects.filter(center=self.center):
                new_meal = Meal(meal_type=option, day=self, center=self.center)
                new_meal.save()

                absent_status = StatusOption.objects.get(name='Absent', center=self.center)  # get the default status option
                for person in Person.objects.filter(center=self.center, active=True, guest=False):  # cycle through all people in the center ignoring inactive people and guests
                    # create new status for each meal and each person
                    Status(status=absent_status, person=person, meal=new_meal).save()

        super().save(*args, **kwargs)  # this has to be called after checking for pk, b/c this creates it

    def __str__(self):
        """Make useful str name"""
        return self.date.strftime("%b %d %Y") + ' - ' + self.center.name


class MealOption(models.Model):
    """A type of meal (b,l,d,etc)"""
    name = models.CharField(max_length=10, default='Breakfast')
    code = models.CharField(max_length=4, default='B')  # code for meal:  B,L,D
    created_at = models.DateTimeField(auto_now_add=True)  # date that person was created
    cutoff_time = models.TimeField(null=True, default=default_cutoff_time)  # time at which meal gets disabled
    cutoff_day = models.IntegerField(default=0)  # number of days before the meal that the cutoff time activates
    meal_time = models.TimeField(null=True)  # time of the meal
    can_edit = models.BooleanField(default=True)

    center = models.ForeignKey(Center, on_delete=models.CASCADE)

    def __str__(self):
        """Make useful str name"""
        return self.name + ' - ' + self.center.name

    def save(self, *args, **kwargs):
        """Overwrite save function to generate meals for the users when a new option is created"""

        if self.pk is None:
            # if there is no id, the instance is new
            super().save(*args, **kwargs)  # must put this here so that person is saved before it is used

            for day in Day.objects.filter(center=self.center):
                # For each day generate a new meal
                absent_status = StatusOption.objects.get(name='Absent', center=self.center)  # get the default status option
                new_meal = Meal(meal_type=self, day=day, center=self.center)
                new_meal.save()
                for person in Person.objects.filter(center=self.center, active=True):  # cycle through all people in the center
                    # create new status for each person
                    Status(status=absent_status, person=person, meal=new_meal).save()

        super().save(*args, **kwargs)


class Meal(models.Model):
    """Models for a datapoint. Contains status for that meal and date of that meal.
    This is the most basic model. This is a specific instance of a meal."""

    # code = models.CharField(max_length=2, default='B')
    updated_at = models.DateTimeField(auto_now=True)
    disabled = models.BooleanField(default=False)  # indicates whether the meal status can be changed
    override = models.CharField(default=None, null=True, max_length=15, blank=True)  # specifies exceptions when counting
    manual_count = models.IntegerField(default=0, null=True, blank=True)  # indicates the number of people present for manual override
    message = models.CharField(default=None, null=True, max_length=300, blank=True)  # message for administration

    center = models.ForeignKey(Center, on_delete=models.CASCADE, default=None, null=True)
    meal_type = models.ForeignKey(MealOption, on_delete=models.CASCADE, null=True)  # Link to mealoption model
    day = models.ForeignKey(Day, on_delete=models.CASCADE, default=None)  # link to Day model

    def __str__(self):
        """Make useful str name"""
        return self.day.date.strftime("%b %d %Y") + ' - ' + self.meal_type.code


class Person(models.Model):
    """Model for an individual. Contains their name, initials, diet. Extends django User class"""
    name = models.CharField(max_length=20, default='', null=True)
    initials = models.CharField(max_length=3, default='', null=True)
    guest = models.BooleanField(default=False)  # if true the user is a guest
    created_at = models.DateTimeField(auto_now_add=True)  # date that person was created
    active = models.BooleanField(default=True)  # if false the user is ignored in everything (basically a way to delete without deleting

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)  # link to user
    center = models.ForeignKey(Center, on_delete=models.CASCADE, null=True)
    diet = models.ForeignKey(Diet, on_delete=models.CASCADE)  # link to diet model
    meals = models.ManyToManyField(Meal)  # for the guest user

    def save(self, *args, **kwargs):
        """Overwrite save function to generate statuses for the new user"""
        if not self.pk:
            # if no id, this is a new instance
            super().save(*args, **kwargs)  # must put this here so that person is saved before itis used
            # if there is no id, the instance is new

            if not self.guest:
                # get all present and future days for the current center
                future_days = self.center.day_set.filter(date__gte=timezone.now().date())
                absent_status = StatusOption.objects.get(name='Absent', center=self.center)  # get the default status option

                for day in future_days:
                    # loop through future days and meals generating statuses for the new person
                    for meal in day.meal_set.all():
                        Status(person=self, meal=meal, status=absent_status).save()

        if self.guest:
            # create status for each meal in the meals field
            present_status = StatusOption.objects.get(name='Present', center=self.center)
            for meal in self.meals.all():
                try:
                    # see if status object exists. If not, create it
                    Status.objects.get(person=self, meal=meal, status=present_status)
                except Status.DoesNotExist:
                    new_status = Status(person=self, meal=meal, status=present_status)
                    new_status.edited_at = timezone.now()
                    new_status.save()

        super().save(*args, **kwargs)

    def __str__(self):
        """Make useful str name"""
        return self.name + ' - ' + self.center.name

    class Meta:
        # Create a custom permission that will be used to customize interface for directors
        permissions = (('is_director', 'Can manage the meal count for the center'),)

class StatusOption(models.Model):
    """A possible option for a meal"""

    name = models.CharField(max_length=50, default='Absent')
    abbreviation = models.CharField(max_length=3, default='O')  # status code string
    priority = models.IntegerField(default=99, blank=True)  # TODO: implement priority: that is the order which options can be selected
    # TODO: implement description
    prep_time = models.IntegerField(default=12)  # number of hours before meal that option gets disabled - Alternative
    can_edit = models.BooleanField(default=True)
    active = models.BooleanField(default=True)  # if active==true, option is available for people in the center (so directors can hide preset options)
    calculated = models.BooleanField(default=False)  # if this is a calculated field

    center = models.ForeignKey(Center, on_delete=models.CASCADE)


    def __str__(self):
        """Make useful str name"""
        return self.name + ' - ' + self.center.name


    # def save(self, *args, **kwargs):
    #     """Overwrite save function to generate statuses for the new user"""
    #
    #     if self.pk is None:
    #         # if there is no id, the instance is new
    #         super().save(*args, **kwargs)  # must put this here so that person is saved before it is used
    #         self.priority = StatusOption.objects.all().count()  # set priority to current length of status set
    #
    #     super().save(*args, **kwargs)

class DisableOption(models.Model):
    """
    A model that can be connected to a StatusOption that contains info on when the meal gets disabled.
    These can be linked to by StatusOption instances.
    """
    name = models.CharField(default='DefaultDisableInstance', max_length=30)
    days_before = models.IntegerField(default=0)  # num of days before that option is disabled
    time = models.TimeField(null=True)  # time that option is disabled

    center = models.ForeignKey(Center, on_delete=models.CASCADE)
    meal_option = models.ManyToManyField(MealOption, blank=True)  # list of meals that this rule applies to
    status_option = models.ManyToManyField(StatusOption, blank=True)  # list of disable rules that are applicable

    def __str__(self):
        """Make useful str name"""
        return self.name + ' - ' + self.center.name

class Status(models.Model):
    """Attributes a person and their status to a meal."""

    # name = models.CharField(max_length=10, default='Absent')
    # abbreviation = models.CharField(max_length=2, default='O')  # status code string

    status = models.ForeignKey(StatusOption, on_delete=models.CASCADE, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    # actual_status is the status of the user inputted during director verification
    actual_status = models.ForeignKey(StatusOption, related_name="actual_status", on_delete=models.CASCADE, null=True, blank=True)

    edited_at = models.DateTimeField(default=None, null=True)

    def save(self, *args, **kwargs):
        """
        Override save method to set the actual_status to the current status upon initilization
        :param args:
        :param kwargs:
        :return:
        """
        if self.pk is None:
            # if there is no id, the instance is new
            #self.actualstatus = self.status
            pass
        super().save(*args, **kwargs)

    def __str__(self):
        """Make useful str name"""
        return self.person.name + ' - ' + str(self.meal)

