# Generated by Django 2.1.5 on 2019-02-03 18:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meal_count', '0019_remove_meal_type_code'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='meal',
            name='cutoff_time',
        ),
        migrations.RemoveField(
            model_name='statusoption',
            name='priority',
        ),
        migrations.AddField(
            model_name='mealoption',
            name='cutoff_time',
            field=models.TimeField(null=True),
        ),
    ]
