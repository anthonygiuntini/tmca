# Generated by Django 2.1.5 on 2019-02-03 17:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meal_count', '0018_meal_type_code'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='meal',
            name='code',
        ),
    ]
