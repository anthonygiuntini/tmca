# Generated by Django 2.1.5 on 2019-01-27 17:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meal_count', '0007_person_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='name',
        ),
    ]
