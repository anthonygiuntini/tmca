#The Meal Count App (Formerly WHAM)

import datetime
import timeit
import pytz
from meal_count import tools
from Wham.settings import *
from Wham.parameters import *

from django.shortcuts import render, get_list_or_404, get_object_or_404, render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.views import auth_login, PasswordResetView
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.utils.translation import activate, get_language_info
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import get_language_from_request, activate
from meal_count.models import Person, Day, Meal, Center, Status, StatusOption, Diet, MealOption
from .forms import CreateUserForm2, NewGuestForm, CreateUserForm1
from .init_app import initialize_app



# NOTE: avoid for loops use complex .filter() calls instead its much faster


def handler404(request, exception, template_name="404.html"):
    response = render_to_response("meal_count/error/404.html")
    response.status_code = 404

    return response


def handler403(request, exception, template_name="403.html"):
    response = render_to_response("meal_count/error/403.html")
    response.status_code = 403
    return response


def handler500(request):  #  exception, template_name="500.html"): This fixes an error on the server side
    response = render_to_response("meal_count/error/500.html")
    response.status_code = 500
    return response


# TODO: hide irrelevant meals/options
####Guest views####

def custom_login(request):
    """
    Use the django auth login view for the login page but redirect to the index after successful login.
    :param request:
    :return:
    """
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    else:
        return auth_login(request)

def custom_pwd_reset(request):
    """
    Use the django auth login view for the login page but redirect to the index after successful login.
    :param request:
    :return:
    """
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    else:
        return PasswordResetView(request)

def clean_url_input(text_str):
    """
    Removes spaces/ capitalization from inputted string.
    :return:
    """
    new_str = text_str.replace(' ', '')  # replaces spaces
    return new_str.lower()

def get_center_from_string(center_n):
    """
    Gets the center from a string.
    :param center_n:
    :return:
    """
    center_name = "null"
    for c in Center.objects.all():
        if clean_url_input(c.name) == clean_url_input(center_n):
            center_name = c.name

    return get_object_or_404(Center, name__iexact=center_name)

def create_user(request, center):
    """Handles the creation of a new user."""

    error_msg = None
    center = get_center_from_string(center)

    if request.method == 'POST':
        # if the form was submitted
        # create model instance
        # TODO: Get first and last name from user instance not person
        # TODO: convert objects.get to .save(commit=false) etc
        user_form = CreateUserForm1(request.POST, prefix='user')
        person_form = CreateUserForm2(request.POST, center_i=center, prefix='person', initial={'active': True, 'center': center})
        if user_form.is_valid():
            print('user_form passed validation')
            user_form.save()

            # TODO: prevent duplicates
            if person_form.is_valid():
                print('person_form passed validation')
                person_obj = person_form.save(commit=False)
                person_obj.center = center
                # FIXME: error handling when duplicate values
                # Get the new person object, connect it to the new user, then save it
                try:
                    user = User.objects.get(username=user_form['username'].value())
                    #person_obj = Person.objects.get(name=person_form['name'].value(), active=True, center=center)
                    # attach user to person object and save
                    person_obj.user = user
                    person_obj.save()
                    # if everything works redirect to login
                    return HttpResponseRedirect('/accounts/login')
                except User.MultipleObjectsReturned:
                    error_msg = _("The username you entered already exists.")
                    print('That username already exists')
                except Person.MultipleObjectsReturned:
                    error_msg = _("The name that you entered is already in the database.")
                    print('That person name already exists')
                except:
                    error_msg = _("Sorry, the account could not be created. Please try again.")

            else:
                #error_msg = person_form.errors
                print('person_from failed validation')

        else:
            error_msg = user_form.errors
            print('user_from failed validation')

    else:
        user_form = CreateUserForm1(prefix='user')  # default user creation form
        person_form = CreateUserForm2(center_i=center, prefix='person')  # custom extension

    # Return create page if it is a GET request or any error occurs
    return render(request, 'registration/create.html', {'user_form': user_form, 'person_form': person_form, 'error_msg':error_msg, 'center_name': center.name})


def count_report(request, center, report_type=None, is_initials=False):
    """Displays the count for the upcoming meals.
    type: if none -> 2 day lookahead; if 'week' -> 7 day lookahead
    is intitals: True: show initials report, False: show just count
    """
    #TODO: require admin priv for initials
    center = get_center_from_string(center)
    center_tz = pytz.timezone(center.timezone)
    now = timezone.localtime(timezone.now(), center_tz)

    if report_type == 'week':
        # look ahead 7x number of meals/day at the center
        look_ahead = len(MealOption.objects.filter(center=center)) * 7
    else:
        # look ahead 2x number of meals/day at the center
        look_ahead = len(MealOption.objects.filter(center=center)) * 2  # number of meals to look ahead

    future_meals = Meal.objects.filter(day__date__gte=now.date(), center=center)
    future_meals = future_meals.order_by('day__date', 'meal_type__meal_time')  # need to sort by date to get them in proper order
    meal_set = future_meals[:look_ahead]

    if is_initials:
        # if it is an initials report set results list to that otherwise do normal report
        results_list = tools.initials(meal_set, center)# [(meal, {diet_name:initials})]
    else:
        results_list = tools.count(meal_set, center)# [(meal, {diet_name:count})]

    ordered_diets = center.diet_set.all().order_by("priority")
    ordered_options = center.statusoption_set.all().order_by("priority")


    num_diets = len(Diet.objects.filter(center=center))  # this is so the title can be centered for each report meal

    context = {'meal_set': meal_set, 'num_diets': num_diets, 'is_initials': is_initials,
               'report_type': report_type, 'center': center,
               'ordered_diets': ordered_diets, 'ordered_options': ordered_options, 'results_list': results_list}

    #Save report page to a html file
    # saved_template_path = os.path.join(SAVED_REPORTS, center.name)
    # if not os.path.exists(saved_template_path):
    #     os.mkdir(saved_template_path)
    # with open(os.path.join(saved_template_path,timezone.datetime.now().strftime("%Y-%m-%d")+'count_report.html'), 'w') as file:
    #     file.write(render_to_string('meal_count/report.html', context, request))
    #   print(render_to_string('meal_count/report.html', context, request))
    return render(request, 'meal_count/report/report.html', context)

# def current_count(request, center):
#     """Displays the count for the upcoming meals."""
#
#     center = get_object_or_404(Center, name__iexact=center)
#     center_tz = pytz.timezone(center.timezone)  # get center's tz, convert it to tz object
#     now = timezone.localtime(timezone.now(), timezone=center_tz)  # use tz aware object for now
#     look_ahead = 12  # number of meals to look ahead
#
#     future_meals = Meal.objects.filter(day__date__gte=now.date())
#     future_meals = future_meals.order_by('day__date')  # need to sort by date to get them in proper order
#     meal_set = future_meals[:look_ahead]
#     results_list = tools.count(meal_set, center)
#
#     num_diets = len(Diet.objects.filter(center=center))  # this is so the title can be centered for each report meal
#
#     return render(request, 'meal_count/main_report.html',
#                   {'meal_set': meal_set, 'results_list': results_list, 'num_diets': num_diets})


def classic_report(request, center):
    """Displays the count for the upcoming meals."""

    center = get_center_from_string(center)
    center_tz = pytz.timezone(center.timezone)  # get center's tz, convert it to tz object
    now = timezone.now()

    future_meals = Meal.objects.filter(day__date__gte=now.date(), center=center)
    future_meals = future_meals.order_by('day__date', 'meal_type__meal_time')  # need to sort by date to get them in proper order
    meal_set = future_meals[:LOOKAHEAD]
    possible_diets = Diet.objects.filter(center=center)  # list of possible diets
    results_list = tools.count(meal_set, center)

    return render(request, 'meal_count/report/Old Report Templates/classic_report.html', {'meal_set': meal_set, 'results_list': results_list,
                                                              'possible_diets': possible_diets})


def redirect_2_lang_url(request):
    """
    Redirects to the url with the browser's language. Useful for the @login_required deorator.
    :param request:
    :return:
    """

    c_lang = get_language_from_request(request)
    activate(c_lang)
    request_dict = request.GET.copy()
    # copy dict so we can modify
    # redirect to the given url name and include the rest of the GET request
    return HttpResponseRedirect('{base_url}?{querystring}'.format(base_url=reverse(request_dict.pop('name')[0]), querystring=request_dict.urlencode()))

####Standard user views####
@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def index(request, week_delta=0):
    """Shows user his own meal count. if no argument passed, it shows current week,
    otherwise it shows today+week_delta. Note week_delta may be negative and is a string"""
    # FIXME: <str:week_delta> receives requests for favicon.ico and others
    # FIXME: when the week_delta goes past the extremes, it keeps counting without changing

    center = request.user.person.center
    center_tz = pytz.timezone(center.timezone)  # get center's tz, convert it to tz object

    try:
        week_delta = float(week_delta)
    except ValueError:
        week_delta = 0
    start_date = timezone.now() + timezone.timedelta(weeks=week_delta)
    stop_date = start_date + timezone.timedelta(weeks=1)
    start_date = timezone.localdate(start_date, center_tz)  # convert to tz aware
    stop_date = timezone.localdate(stop_date, center_tz)  # convert to tz aware

    # get all days in the next week
    days = Day.objects.filter(center=center, date__range=(start_date, stop_date))
    if len(days) < 7:
        # if it shows less than a week adjust the dates so a whole week is shown
        # Note the long .localize(combine... is required to convert date to datetime then naive to tz aware
        if week_delta < 0:
            start_date = center_tz.localize(datetime.datetime.combine(Day.objects.filter(center=center).earliest('date').date, datetime.time()))  # get oldest day and use as min
            start_date = timezone.localdate(start_date, center_tz)  # convert to tz aware
            stop_date = start_date + timezone.timedelta(weeks=1)
            stop_date = center_tz.localize(datetime.datetime.combine(stop_date, datetime.time()))  # convert to tz aware
        else:
            stop_date = center_tz.localize(datetime.datetime.combine(Day.objects.filter(center=center).latest('date').date, datetime.time()))  # get oldest day and use as min
            stop_date = timezone.localdate(stop_date, center_tz)  # convert to tz aware
            start_date = stop_date - timezone.timedelta(weeks=1)
            start_date = center_tz.localize(datetime.datetime.combine(start_date, datetime.time())) # convert to tz aware

        days = Day.objects.filter(center=center, date__range=(start_date, stop_date))

    days = days.order_by('date')  # order the days

    current_person = request.user.person
    tools.disable_meals(current_person)
    # tools.delete_expired_guests()
    #TODO: this is temporary until people have filled out the defaults
    tools.auto_fill_status(current_person, days)
    all_options = StatusOption.objects.filter(center=center, active=True, calculated=False)
    all_abbr = [abbr.abbreviation for abbr in all_options]
    options = {'days': days,
               'person': current_person,
               'all_options': all_abbr,
               'week_delta': week_delta,
               'today': timezone.localtime(timezone.now(), center_tz).date(),
               'time': timezone.localtime(timezone.now(), center_tz)}

    return render(request, 'meal_count/status/index.html', options)


@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def index_default(request):
    """Shows user his own default meal count."""
    #NOTE: SUPERCEDED BY COUNT

    center = request.user.person.center
    center_tz = pytz.timezone(center.timezone)  # get center's tz, convert it to tz object

    start_date = timezone.datetime(year=DEFAULT_YEAR, month=DEFAULT_MONTH, day=DEFAULT_DAY)  #Date of OF's cannonization 10/06/2002
    stop_date = start_date + timezone.timedelta(weeks=1)
    # start_date = timezone.localdate(start_date, center_tz)  # convert to tz aware
    # stop_date = timezone.localdate(stop_date, center_tz)  # convert to tz aware

    # get all days in the next week
    days = Day.objects.filter(center=center, date__range=(start_date, stop_date))

    # If the default week does not exist, create it
    if len(days) != 7:
        for i in range(7):
            # go through all 7 days creating each day
            c_date = start_date + timezone.timedelta(days=i)
            # c_date = timezone.localdate(c_date, center_tz)  # convert to tz aware
            new_day = Day(center=center, date=c_date, message=_("Default"))
            new_day.save()

    days = Day.objects.filter(center=center, date__range=(start_date, stop_date))
    days = days.order_by('date')  # order the days

    current_person = request.user.person

    default_statuses = Status.objects.filter(person=current_person, meal__day__in=days)
    if len(default_statuses) == 0:
        # if the user does not have default statii create them
        default_meals = Meal.objects.filter(day__in=days, center=center)  # list of default meals
        absent_status = StatusOption.objects.get(name='Absent', center=center)
        for meal in default_meals:
            new_status = Status(person=current_person, meal=meal, status=absent_status, actual_status=absent_status)
            new_status.save()

    #tools.disable_meals(current_person)
    # tools.delete_expired_guests()
    # tools.auto_fill_status(current_person, days)
    all_options = StatusOption.objects.filter(center=center, active=True, calculated=False)
    all_abbr = [abbr.abbreviation for abbr in all_options]
    options = {'days': days,
               'person': current_person,
               'all_options': all_abbr,
               'week_delta': 'default',
               'today': timezone.localtime(timezone.now(), center_tz).date()}

    return render(request, 'meal_count/status/index.html', options)

@login_required
def initials_report(request, center, report_type=None):
    """Displays the count for the upcoming meals."""

    center = get_center_from_string(center)
    center_tz = pytz.timezone(center.timezone)  # get center's tz, convert it to tz object
    now = timezone.localtime(timezone.now(), timezone=center_tz)  # use tz aware object for now

    if report_type == 'week':
        # look ahead 7x number of meals/day at the center
        look_ahead = len(MealOption.objects.filter(center=center)) * 7
    else:
        # look ahead 2x number of meals/day at the center
        look_ahead = len(MealOption.objects.filter(center=center)) * 2  # number of meals to look ahead

    future_meals = Meal.objects.filter(day__date__gte=now.date(), center=center)
    future_meals = future_meals.order_by('day__date', 'meal_type__meal_time')  # need to sort by date to get them in proper order
    meal_set = future_meals[:look_ahead]
    results_list = tools.initials(meal_set, center)

    num_diets = len(Diet.objects.filter(center=center))  # this is so the title can be centered for each report meal

    meal_option = []  # [(meal, option)]
    counts = []  # [[(diet,count)]]

    for result in results_list:
        for option in result[1].keys():
            # Add the meal/option name pair to the meal_option list
            # NOTE: need model_to_dict to make the whole list convertible to json
            meal_o_dict = model_to_dict(result[0])
            meal_o_dict['day'] = str(Day.objects.get(id=meal_o_dict['day']).date)  # replace day id with date
            meal_o_dict['name'] = str(MealOption.objects.get(id=meal_o_dict['meal_type']).name)  # insert name field with name of meal_type
            meal_option.append((meal_o_dict, option))
            # then add the diet/count pair to the counts list
            diets_dict = result[1][option]
            tmp_list = []
            for diet in diets_dict.keys():
                tmp_list.append((diet, diets_dict[diet]))
            counts.append(tmp_list)

    meal_option = tools.make_json(meal_option)
    counts = tools.make_json(counts)
    context = {'meal_set': meal_set, 'meal_option': meal_option, 'counts': counts, 'num_diets': num_diets,
               'is_initials': 'True', 'center': center.name, 'report_type': report_type}

    return render(request, 'meal_count/report/report.html', context)

# @login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
# def initials_report(request, center):
#     """Displays the count for the upcoming meals."""
#
#     center = get_object_or_404(Center, name__iexact=center)
#     center_tz = pytz.timezone(center.timezone)  # get center's tz, convert it to tz object
#
#     now = timezone.localtime(timezone.now(), center_tz)
#
#     future_meals = Meal.objects.filter(day__date__gte=now.date())
#     future_meals = future_meals.order_by('day__date')  # need to sort by date to get them in proper order
#     meal_set = future_meals[:LOOKAHEAD]
#     results_list = tools.initials(meal_set, center)
#
#     num_diets = len(Diet.objects.filter(center=center))  # this is so the title can be centered for each report meal
#
#     return render(request, 'meal_count/initials_report.html',
#                   {'meal_set': meal_set, 'results_list': results_list, 'num_diets': num_diets})


@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def guests(request):
    """Allows user to add guests"""

    center = request.user.person.center
    guest_qs = Person.objects.filter(center=center, guest=True, active=True)
    diets = Diet.objects.filter(center=center)
    now = timezone.now()

    meals = Meal.objects.filter(center=center, day__date__gte=now)
    meals = meals.order_by('day__date', 'meal_type__meal_time')  # order by the date and time of the meal

    if request.method == 'POST':
        guest_form = NewGuestForm(request.POST, user=request.user)
        if guest_form.is_valid():
            # TODO: check for duplicates
            guest_obj = guest_form.save(commit=False)

            guest_obj.center = center
            guest_obj.guest = True
            guest_obj.active = True

            guest_obj.save()
            guest_form.save_m2m()  # need this to save many to many field after the model's .save method
            guest_obj.save()  # then call the model's save method again to create the statuses

            ROUTINE_LOGGER.info('User "'+request.user.username+'" created guest "'+guest_obj.name+'"')
    else:

        guest_form = NewGuestForm(user=request.user)

    options = {'guests': guest_qs,
               'diets': diets,
               'meals': meals,
               'guest_form': guest_form,
               }

    return render(request, 'meal_count/Management/guests.html', options)


@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def delete_guest(request):
    """Allows director to delete a status option"""
    # TODO: look into effects of deleting diets
    center = request.user.person.center
    error = None

    if request.method == 'POST':
        # if the add option form was submitted
        guest_id = request.POST['guest_id']
        if guest_id != '':
            # prevent blank submissions

            try:
                guest = Person.objects.get(id=guest_id, active=True)
                guest.delete()
                ROUTINE_LOGGER.info('User "' + request.user.username + '" deleted guest "' + guest.name+'"')
            except Person.DoesNotExist:
                return HttpResponse('failure')

    return HttpResponse('Success')


@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def update_status(request):
    """Receives POST request with status_id and new status."""
    # Confirm that it was a post request
    if request.method == 'POST':
        print('update_status view: received post request.')
        status_id = request.POST.get('status_id', '')  # get status id
        status_val = request.POST.get('status_val', '')  # get status value
        now = timezone.now()

        if status_id != '' and status_val != '':
            # If they're not blank
            try:
                status_i = Status.objects.get(id=status_id)
                prev_status_n = status_i.status.name
            except ObjectDoesNotExist:
                print('update_status view: object not found error')

            meal = status_i.meal

            # check if its a quick select val, if so, set the new status val directly
            if status_val[:3] == 'qs_':
                next_status = status_val[3:]
            else:
                avail_opts = tools.available_options(meal, status_i.person)

                if status_val not in avail_opts:
                    # ensure that the current status is in the list of available options
                    avail_opts.append(status_val)

                # increment the current index
                c_index = avail_opts.index(status_val)

                if c_index < len(avail_opts) - 1:
                    next_status = avail_opts[c_index + 1]
                else:
                    next_status = avail_opts[0]

            statusoption_i = StatusOption.objects.get(abbreviation=next_status, center=meal.center)

            status_i.status = statusoption_i
            status_i.edited_at = timezone.now()
            status_i.save()

            ROUTINE_LOGGER.info('User "'+request.user.username+'" updated "'+str(status_i) +\
                                '" to "' + status_i.status.name+'" from "'+prev_status_n+'"')

        return HttpResponse('s' + next_status)
    else:
        return HttpResponse('e')


@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def account_view(request):
    """
    TODO: make account page. For now it redirects to home.
    :param request:
    :return:
    """

    return HttpResponseRedirect('/')


@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def logout_user(request):
    """
    Logs the user out, then redirects to login screen
    :param request:
    :return:
    """
    # log the user out
    logout(request)

    return HttpResponseRedirect('/accounts/login')

@login_required(login_url='/redirect_to_lang_url/?name=meal_count:auth:auth_login')
def set_email(request):
    """
    Sets the user's email.
    :param request:
    :return:
    """
    if request.method == 'POST':
        # if the add option form was submitted
        email_addr = request.POST['email_addr']

        user = request.user
        user.email = email_addr
        user.save()

    return HttpResponseRedirect('/')

####Meal Czar views####

@permission_required('person.is_director')
def view_all(request, week_delta=0):
    """Shows the mealcounts for all users for that center. Note week_delta may be negative and is a string"""
    # FIXME: <str:week_delta> receives requests for favicon.ico and others
    # FIXME: when the week_delta goes past the extremes, it keeps counting without changing

    center = request.user.person.center

    try:
        week_delta = float(week_delta)
    except ValueError:
        week_delta = 0
    start_date = timezone.now() + timezone.timedelta(weeks=week_delta)
    stop_date = start_date + timezone.timedelta(weeks=1)

    # get all days in the next week
    days = Day.objects.filter(center=center, date__range=(start_date, stop_date))
    if len(days) < 7:
        # if it shows less than a week adjust the dates so a whole week is shown
        if week_delta < 0:
            start_date = Day.objects.filter(center=center).earliest('date').date  # get oldest day and use as min
            stop_date = start_date + timezone.timedelta(weeks=1)
        else:
            stop_date = Day.objects.filter(center=center).latest('date').date  # get oldest day and use as min
            start_date = stop_date - timezone.timedelta(weeks=1)

        days = Day.objects.filter(center=center, date__range=(start_date, stop_date))

    days = days.order_by('date')  # order the days
    people = Person.objects.filter(center=center, active=True)
    # TODO: check to see if sorting works
    people = people.order_by('initials', 'guest')  # Put guests at the end sort by name

    #tools.disable_meals(center=center)
    # tools.delete_expired_guests()
    # tools.auto_fill_status(people, days)
    all_options = StatusOption.objects.filter(center=center, active=True, calculated=False)
    all_abbr = [abbr.abbreviation for abbr in all_options]
    options = {'days': days,
               'people': people,
               'all_options': all_abbr,
               'week_delta': week_delta,
               'today': timezone.now().date(),
               'overrides': AVAILABLE_OVERRIDES
               }

    return render(request, 'meal_count/status/view_all.html', options)


@permission_required('person.is_director')
def meals(request, week_delta=0):
    """Shows user his own meal count. if no argument passed, it shows current week,
    otherwise it shows today+week_delta. Note week_delta may be negative and is a string"""
    # FIXME: <str:week_delta> receives requests for favicon.ico and others
    # FIXME: when the week_delta goes past the extremes, it keeps counting without changing

    center = request.user.person.center

    try:
        week_delta = float(week_delta)
    except ValueError:
        week_delta = 0
    start_date = timezone.now() + timezone.timedelta(weeks=week_delta)
    stop_date = start_date + timezone.timedelta(weeks=1)

    # get all days in the next week
    days = Day.objects.filter(center=center, date__range=(start_date, stop_date))
    if len(days) < 7:
        # if it shows less than a week adjust the dates so a whole week is shown
        if week_delta < 0:
            start_date = Day.objects.filter(center=center).earliest('date').date  # get oldest day and use as min
            stop_date = start_date + timezone.timedelta(weeks=1)
        else:
            stop_date = Day.objects.filter(center=center).latest('date').date  # get oldest day and use as min
            start_date = stop_date - timezone.timedelta(weeks=1)

        days = Day.objects.filter(center=center, date__range=(start_date, stop_date))

    days = days.order_by('date')  # order the days
    people = Person.objects.filter(center=center, active=True)

    # Get list of all meals that are in the day range and have the people
    meals = Meal.objects.filter(center=center, day__in=days)

    # TODO: check to see if sorting works
    people = people.order_by('initials', 'guest')  # Put guests at the end sort by name

    # tools.disable_meals(center=center)
    # tools.delete_expired_guests()
    # tools.auto_fill_status(people, days)
    all_options = StatusOption.objects.filter(center=center, active=True, calculated=False)
    all_abbr = [abbr.abbreviation for abbr in all_options]
    options = {'meals': meals,
               'days': days,
               'people': people,
               'all_options': all_abbr,
               'week_delta': week_delta,
               'today': timezone.now().date(),
               'overrides': AVAILABLE_OVERRIDES
               }

    return render(request, 'meal_count/status/meals.html', options)


@permission_required('person.is_director')
def view_all_defaults(request):
    """Shows the mealcounts for all users for that center. Note week_delta may be negative and is a string"""
    # FIXME: <str:week_delta> receives requests for favicon.ico and others
    # FIXME: when the week_delta goes past the extremes, it keeps counting without changing

    center = request.user.person.center

    start_date = timezone.datetime(year=DEFAULT_YEAR, month=DEFAULT_MONTH, day=DEFAULT_DAY)
    stop_date = start_date + timezone.timedelta(weeks=1)

    # get all days in the next week
    days = Day.objects.filter(center=center, date__range=(start_date, stop_date))

    days = days.order_by('date')  # order the days
    people = Person.objects.filter(center=center, active=True)
    people = people.order_by('initials', 'guest')  # Put guests at the end sort by name

    #tools.disable_meals(center=center)
    # tools.delete_expired_guests()
    # tools.auto_fill_status(people, days)
    all_options = StatusOption.objects.filter(center=center, active=True, calculated=False)
    all_abbr = [abbr.abbreviation for abbr in all_options]
    options = {'days': days,
               'people': people,
               'all_options': all_abbr,
               'week_delta': 'default',
               'today': timezone.now().date(),
               'overrides': AVAILABLE_OVERRIDES
               }

    return render(request, 'meal_count/status/view_all.html', options)


@permission_required('person.is_director')
def verify_attendance(request, current_meal=None):
    """Allows director to verify if a person's status is correct"""

    center = request.user.person.center
    center_tz = pytz.timezone(center.timezone)

    now = timezone.localtime(timezone.now(), center_tz)

    # Get all active people from that center who are not guests
    people = Person.objects.filter(center=center, active=True, guest=False)
    meals = Meal.objects.filter(center=center, day__date=now.date())  # get all the meals for that center that day
    options = StatusOption.objects.filter(center=center, name__in=VERIFY_OPTIONS, active=True, calculated=False)
    statii = Status.objects.filter(meal__in=meals, person__in=people)  # get all statuses from meals with people

    if current_meal is None:
        # if there was no meal specified, set the current meal to the closest meal in the past
        meals.order_by('meal_type__meal_time')
        smallest_timedelta = datetime.timedelta(days=-2)  # will all be negative
        for meal in meals:
            # go through meals keeping track of 1) if the meal is in the past, 2) the shortest timedelta between now and then
            meal_dt = timezone.datetime.combine(timezone.localtime(timezone.now(), center_tz).date(), meal.meal_type.meal_time)
            timedelta = meal_dt.replace(tzinfo=center_tz) - now  # NOTE: need to add tz info for the meal_time otherwise it will calculate as utc tz
            if datetime.timedelta(hours=0) > timedelta > smallest_timedelta:
                # Save the least negative number
                smallest_timedelta = timedelta
                current_meal = meal.meal_type.name

    tools.init_actual_status(statii)
    options = {
        'people': people,
        'meals': meals,
        'statii': statii,
        'options': options,
        'today': timezone.localtime(timezone.now(), center_tz).date(),
        'current_meal': current_meal,
        }

    return render(request, 'meal_count/status/verify_attendance.html', options)

@permission_required('person.is_director')
def set_act_status(request):
    """Receives POST request with status_id and new status."""

    center = request.user.person.center

    # Confirm that it was a post request
    if request.method == 'POST':
        print('set_act_status view: received post request.')
        status_id = request.POST.get('status_id', '')  # get status id
        status_val = request.POST.get('status_val', '')  # get status value
        now = timezone.now()

        if status_id != '' and status_val != '':
            # If they're not blank
            try:
                status_i = Status.objects.get(id=status_id)
                prev_status_n = status_i.status.name
            except ObjectDoesNotExist:
                print('update_status view: object not found error')

            meal = status_i.meal

            avail_opts = StatusOption.objects.filter(center=center, name__in=VERIFY_OPTIONS, active=True).values_list('abbreviation', flat=True)

            # manually create a list of abbreviations for the available options

            # increment the current index
            c_index = list(avail_opts).index(status_val)

            if c_index < len(avail_opts) - 1:
                next_status = avail_opts[c_index + 1]
            else:
                next_status = avail_opts[0]

            statusoption_i = StatusOption.objects.get(abbreviation=next_status, center=center)

            status_i.actual_status = statusoption_i
            status_i.save()

            ROUTINE_LOGGER.info('User "'+request.user.username+'" changed actual status "'+str(status_i) +\
                                '" to "' + status_i.status.name+'" from "'+prev_status_n+'"')

        prefix = 'n'
        if status_i.actual_status == status_i.status:
            # if the actual status matches the user-set status set prefix to nm
            prefix = 'y'
        else:
            # otherwise assume it is different
            prefix = 'n'
        return HttpResponse(prefix + next_status)
    else:
        return HttpResponse('e')

@permission_required('person.is_director')
def generate_dates(request):
    """Generates a week of days and meals automatically."""

    tools.generate_dates()

    return HttpResponse('Dates generated. <br> Return <a href="/">home</a>')


@permission_required('person.is_director')
def manage(request):
    """Allows director to manage meal count. E.g. add options, reset stats, change settings, etc."""
    center = request.user.person.center
    error = None

    return render(request, 'meal_count/manage.html', {'center': center, 'error': error})

@permission_required('person.is_director')
def manage_users(request, username=None):
    """Allows director to manage meal count. E.g. add options, reset stats, change settings, etc."""
    center = request.user.person.center
    error = None
    main_list = center.person_set.all().order_by('name')
    pagetype = 'users'  # indicates that the page is a user management page
    if username is not None:
        # prefetch related so that code can access Person object without additional querys
        selected_user = get_object_or_404(User.objects.prefetch_related(), username=username)
    else:
        selected_user = None

    context = {'center': center, 'error': error, 'main_list': main_list, 'current_item': selected_user,
               'pagetype': pagetype}
    return render(request, 'meal_count/manage_users.html', context)


@permission_required('person.is_director')
def change_person_status(request):
    """
    Toggles the active state of the option.
    :param request:
    :return:
    """
    center = request.user.person.center
    error = None
    if request.method == 'POST':
        person_id = request.POST['person_id']
        person_status = request.POST['person_status']
        try:
            person = Person.objects.get(id=person_id)
            print(person_status)
            if person_status == 'true':
                person.active = True
            else:
                person.active = False
        except Person.DoesNotExist:
            error = ''
            ERROR_LOGGER.error('change_person_status: Person with id '+str(person_id)+' not found.')
        except:
            error = 'An unknown error occurred'

        person.save()
        print('change_person_status: ' + person.name + ' is ' + str(person.active))

    if error:
        # if there's an error return the error
        return HttpResponse(error)
    else:
        return HttpResponse('success')


@permission_required('person.is_director')
def toggle_status_option(request):
    """
    Toggles the active state of the option.
    :param request:
    :return:
    """
    center = request.user.person.center
    error = None
    if request.method == 'POST':
        opt_code = request.POST['status_id']
        option = StatusOption.objects.get(id=opt_code)
        option.active = not option.active
        option.save()

    if error:
        # if there's an error return the error
        return HttpResponse(error)
    else:
        return HttpResponse('success')

@permission_required('person.is_director')
def add_option(request):
    """Allows director to manage the status options"""
    center = request.user.person.center
    error = None
    # TODO: create separate view for adding options (like an api)
    if request.method == 'POST':
        # if the add option form was submitted
        opt_name = request.POST['opt_name']
        opt_code = request.POST['opt_code']
        opt_preptime = request.POST['opt_preptime']

        if opt_name != '' and opt_code != '':
            # prevent blank submissions
            if not StatusOption.objects.filter(name=opt_name, center=center).exists():
                # prevent duplicate name submissions
                if not StatusOption.objects.filter(abbreviation=opt_code, center=center).exists():
                    # prevent duplicate code submissions
                    opt_count = StatusOption.objects.filter(center=center).count()  # get the current count: this will be the priority value
                    StatusOption(name=opt_name, abbreviation=opt_code, priority=opt_count, prep_time=opt_preptime,
                                 center=center, active=True).save()
                else:
                    error = 'Please choose a different option code.'
            else:
                error = 'Please choose a different option name.'
        else:
            error = 'Please fill out all cells.'

    if error:
        # if there's an error return the error
        return HttpResponse(error)
    else:
        return HttpResponse('success')


@permission_required('person.is_director')
def add_diet(request):
    """Allows director to manage the status options"""
    center = request.user.person.center
    diet_error = None
    # TODO: create separate view for adding options (like an api)
    if request.method == 'POST':
        # if the add option form was submitted
        diet_name = request.POST['diet_name']
        diet_code = request.POST['diet_code']
        diet_description = request.POST['diet_description']

        if diet_name != '' and diet_code != '' and diet_description != '':
            # prevent blank submissions
            if not Diet.objects.filter(name=diet_name, center=center).exists():
                # prevent duplicate name submissions
                if not Diet.objects.filter(code=diet_code, center=center).exists():
                    # prevent duplicate code submissions
                    Diet(name=diet_name, code=diet_code, description=diet_description, center=center).save()
                else:
                    diet_error = 'Please choose a different option code.'
            else:
                diet_error = 'Please choose a different option name.'
        else:
            diet_error = 'Please fill out all cells.'

    if diet_error:
        # if there's an error return the error
        return HttpResponse(diet_error)
    else:
        return HttpResponse('success')


@permission_required('person.is_director')
def delete_diet(request):
    """Allows director to delete a status option"""
    # TODO: look into effects of deleting diets
    center = request.user.person.center
    error = None

    if request.method == 'POST':
        # if the add option form was submitted
        diet_id = request.POST['diet_id']
        if diet_id != '':
            # prevent blank submissions

            try:
                diet = Diet.objects.get(id=diet_id)
                diet.delete()
            except Diet.DoesNotExist:
                return HttpResponse('failure')

    return HttpResponse('Success')


@permission_required('person.is_director')
def add_meal(request):
    """Allows director to manage the status options"""
    center = request.user.person.center
    meal_error = None
    # TODO: create separate view for adding options (like an api)
    if request.method == 'POST':
        # if the add option form was submitted
        meal_name = request.POST['meal_name']
        meal_code = request.POST['meal_code']
        meal_time = request.POST['meal_time']
        #meal_cutoff = request.POST['meal_cutoff']
        #meal_cutoff_day = request.POST['meal_cutoff_day']
        # meal_cutoff = None
        if meal_name != '' and meal_code != '':
            # prevent blank submissions
            if not MealOption.objects.filter(name=meal_name, center=center).exists():
                # prevent duplicate name submissions
                if not MealOption.objects.filter(code=meal_code, center=center).exists():
                    # prevent duplicate code submissions
                    MealOption(name=meal_name, code=meal_code, meal_time=meal_time, center=center).save()
                else:
                    meal_error = 'Please choose a different option code.'
            else:
                meal_error = 'Please choose a different option name.'
        else:
            meal_error = 'Please fill out all cells.'

    if meal_error:
        # if there's an error return the error
        return HttpResponse(meal_error)
    else:
        return HttpResponse('success')


@permission_required('person.is_director')
def delete_meal(request):
    """Allows director to delete a status option"""
    # TODO: look into effects of deleting meals
    center = request.user.person.center
    error = None

    if request.method == 'POST':
        # if the add option form was submitted
        meal_id = request.POST['meal_id']
        if meal_id != '':
            # prevent blank submissions

            try:
                meal = MealOption.objects.get(id=meal_id)
                meal.delete()
            except MealOption.DoesNotExist:
                return HttpResponse('failure')

    return HttpResponse('Success')


@permission_required('person.is_director')
def delete_option(request):
    """Allows director to delete a status option"""
    # FIXME: deleting status options in use breaks meals
    center = request.user.person.center
    error = None

    if request.method == 'POST':
        # if the add option form was submitted
        opt_id = request.POST['opt_id']
        if opt_id != '':
            # prevent blank submissions

            try:
                status_option = StatusOption.objects.get(id=opt_id, center=center)
                status_option.delete()
            except StatusOption.DoesNotExist:
                return HttpResponse('failure')

    return HttpResponse('Success')


@permission_required('person.is_director')
def add_message(request):
    """
    Adds a message to the meal. For Ajax.
    :param request:
    :return:
    """

    if request.method == 'POST':
        # if the add option form was submitted
        day_id = request.POST['day_id']
        message = request.POST['message']
        msg_type = request.POST.get('msg_type')
        if day_id != '' and msg_type == 'day':
            # prevent blank submissions

            try:
                day = Day.objects.get(id=day_id)
                day.message = message
                day.save()
            except Day.DoesNotExist:
                return HttpResponse('failure')

        elif msg_type == 'meal' and day_id != '':
            # if its a meal message
            try:
                meal = Meal.objects.get(id=day_id)
                meal.message = message
                meal.save()
            except Day.DoesNotExist:
                return HttpResponse('failure')

    return HttpResponse('success')


@permission_required('person.is_director')
def set_override(request):
    """
    Sets the override for a meal. For Ajax.
    :param request:
    :return:
    """

    if request.method == 'POST':
        # if the add option form was submitted
        meal_id = request.POST['meal_id']
        override = request.POST['override_val']
        manual_count = request.POST['manual_count']

        if meal_id != '':
            # prevent blank submissions

            try:

                meal = Meal.objects.get(id=meal_id)
                meal.override = override
                if manual_count is not None and override == "Manual Count":
                    # if its a manual count override add the count to the meal
                    meal.manual_count = manual_count
                meal.save()
            except Meal.DoesNotExist:
                return HttpResponse('failure')

    return HttpResponse('success')


####Admin/Backend Views####


def initialize_meal_count(request):
    """Runs the utils.initialize function"""
    initialize_app()
    return HttpResponse('Meal count initialized. <br>Return <a href="/">home</a>')
