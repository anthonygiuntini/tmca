#Collection of custom functions

from django.contrib.auth.models import User, Group, Permission
from django.db import IntegrityError


"""
After a db flush:
1) python3 manage.py createsuperuser --settings=Wham.deploy_settings
2) visit /init-meal-count
3) go to django admin and create center object
4) go to /generate
4) attach person to admin account (if applicable)
"""


def initialize_app():
    """In the event of a hard reset (database cleared), do these items"""
    # Need to assign center to admin user for testing
    # Need to assign model permissions to director group

    try:
        directors_group = Group(name="Directors") #create permissions group for directors
        directors_group.save()
        is_director_perm = Permission.objects.get(codename='is_director')
        directors_group.permissions.add(is_director_perm)
    except IntegrityError:
        print('initialize_app: directors group already created')


