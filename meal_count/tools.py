import datetime
import pytz
import json
from collections import Counter, defaultdict
from itertools import chain
from Wham.parameters import *
from django.utils import timezone
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from .models import MealOption, Meal, Person, Status, Diet, StatusOption, Center, Day, DisableOption
from django.utils.translation import ugettext_lazy as _

# TODO: look into the effect on speed that not distinguishing between centers has for the following 2 functions

def current_localtime(ctimezone):
    """
    Returns the localtime
    :param ctimezone:
    :return:
    """
    center_tz = pytz.timezone(ctimezone)
    now = timezone.localtime(timezone.now(), center_tz)

    return now


def delete_expired_guests():
    """Delete guests who don't have any active meals."""
    all_guests = Person.objects.filter(guest=True, active=True)

    for guest in all_guests:
        center_tz = pytz.timezone(guest.center.timezone)
        now = timezone.localtime(timezone.now(), center_tz)

        # get all meals on today's date or later
        active_meals = guest.status_set.filter(meal__day__date__gte=now.date())
        # if there aren't meals today or later delete the guest
        if len(active_meals) == 0:
            guest.delete()
            print('delete_expired_guests deleted: ' + guest.name)
            ROUTINE_LOGGER.info('delete_expired_guests deleted: ' + guest.name)


def available_options(meal, person):
    """
    returns a list of available meal option abbreviations by the disable time and date fields.
    :param meal:
    :param person:
    :return:
    """
    avail_options = []

    # get the meal datetime
    meal_time = meal.meal_type.meal_time  # get time of the selected meal
    meal_day = meal.day.date  # get date of the selected meal
    meal_datetime = datetime.datetime.combine(meal_day, meal_time)

    tz = person.center.timezone  # get user's tz string
    user_tz_i = pytz.timezone(tz)  # convert to tz object
    meal_datetime = user_tz_i.localize(meal_datetime)  # must convert to tz aware datetime
    now = timezone.localtime(timezone.now(), user_tz_i)

    all_options = StatusOption.objects.filter(center=person.center, active=True, calculated=False)
    if meal.day.message == "Default" or meal.meal_type.disableoption_set.count() == 0:
        # if its a default day or there are no disable rules, don't disable anything
        for option in all_options:
            avail_options.append(option.abbreviation)
    else:
        for option in all_options:
            # get the time that the option is disabled
            #disabled_datetime = meal_datetime - timezone.timedelta(hours=option.prep_time)
            try:
                # get disable object for the option at the given meal
                current_disable_rule = option.disableoption_set.get(meal_option=meal.meal_type)
            except DisableOption.MultipleObjectsReturned:
                # if multiple objects returned, log it and go with first object
                print('Tools.py - available_options: multiple DisableAt objects returned')
                ROUTINE_LOGGER.error('Multiple DisableAt objects returned for meal '+meal.meal_type.name+' option '+option.name)
                #TODO: make resolving conflicts smarter: eg go with earlier option in conflicts
                current_disable_rule = option.disableoption_set.filter(meal_option=meal.meal_type).first()
            except DisableOption.DoesNotExist:
                current_disable_rule = None

            if current_disable_rule is None:
                # if there is no rule for that option, add the option
                avail_options.append(option.abbreviation)
            else:
                # otherwise determine if the rule disables it
                disabled_date = meal_datetime.date() - timezone.timedelta(days=current_disable_rule.days_before)
                disabled_time = current_disable_rule.time

                disabled_datetime = user_tz_i.localize(timezone.datetime.combine(disabled_date, disabled_time))
                # compare it to the current time
                if disabled_datetime > now:
                    # the meal is disabled after the current time add it to the list
                    avail_options.append(option.abbreviation)
                else:
                    pass

    return avail_options


def available_options_by_prep_time(meal, person):
    """Returns a list of available meal option abbreviations by the prep time field.
    Note: the person object is used to simplify future features"""
    # TODO: change for person specific options

    avail_options = []
    # get the meal datetime
    meal_time = meal.meal_type.meal_time  # get time of the selected meal
    meal_day = meal.day.date  # get date of the selected meal
    meal_datetime = datetime.datetime.combine(meal_day, meal_time)
    tz = person.center.timezone  # get user's tz string
    user_tz_i = pytz.timezone(tz)  # convert to tz object
    meal_datetime = user_tz_i.localize(meal_datetime)  # must convert to tz aware datetime
    now = timezone.now()

    all_options = StatusOption.objects.filter(center=person.center, active=True, calculated=False)

    if meal.day.message == "Default":
        # if its a default day, don't disable anything
        for option in all_options:
            avail_options.append(option.abbreviation)
    else:
        for option in all_options:
            # get the time that the option is disabled
            disabled_datetime = meal_datetime - timezone.timedelta(hours=option.prep_time)

            # compare it to the current time
            if disabled_datetime > now:
                # the meal is disabled after the current time add it to the list
                avail_options.append(option.abbreviation)
            else:
                pass

    return avail_options


def auto_fill_status(person_input, days):
    """
    Automatically fills the status with the default week's status
    :person: can be a single person instance or a queryset (since if it is an admin, it does it for all)
    :return:
    """
    if isinstance(person_input, QuerySet):
        #  if person is a queryset pass
        pass
    else:
        #  otherwise make person into a single item list
        person_input = [person_input]
    for person in person_input:
        for day in days:
            #  iterate through the days then get all the status' for each day
            persons_statuses = Status.objects.filter(meal__day=day, person=person)

            day_date = DEFAULT_DAY + day.date.weekday() + 1 # need +1 b/c default_day is sunday, but .weekday starts on Monday
            if day_date > DEFAULT_DAY + 6:
                # The +1 causes day_date to skip sunday, this corrects that
                day_date = DEFAULT_DAY

            default_date = timezone.datetime(year=DEFAULT_YEAR, month=DEFAULT_MONTH, day=day_date)

            for status in persons_statuses:
                #  loop through each status
                if status.edited_at is None:
                    #  if it is none set its value to the previous week's
                    try:
                        # get the status from the previous week for that person for the same meal type
                        def_week_stat = Status.objects.get(person=person, meal__day__date=default_date,
                                                           meal__meal_type=status.meal.meal_type)
                        #  set the current status to the previous one and save it
                        status.status = def_week_stat.status
                        status.save()

                    except Status.DoesNotExist:
                        #  if the previous week value doesnt exist ignore it
                        pass


def auto_fill_status_prev_week(person_input, days):
    """
    Automatically fills the status with the previous week's status if not edited.
    :person: can be a single person instance or a queryset (since if it is an admin, it does it for all)
    :return:
    """
    if isinstance(person_input, QuerySet):
        #  if person is a queryset pass
        pass
    else:
        #  otherwise make person into a single item list
        person_input = [person_input]
    for person in person_input:
        for day in days:
            #  iterate through the days then get all the status' for each day
            persons_statuses = Status.objects.filter(meal__day=day, person=person)

            prev_week_day = day.date - timezone.timedelta(weeks=1)

            for status in persons_statuses:
                #  loop through each status
                if status.edited_at is None:
                    #  if it is none set its value to the previous week's
                    try:
                        # get the status from the previous week for that person for the same meal type
                        prev_week_stat = Status.objects.get(person=person, meal__day__date=prev_week_day,
                                                            meal__meal_type=status.meal.meal_type)
                        #  set the current status to the previous one and save it
                        status.status = prev_week_stat.status
                        status.save()

                    except Status.DoesNotExist:
                        #  if the previous week value doesnt exist ignore it
                        pass


def disable_meals(person=None, center=None):
    """Disables meals that have no available options."""
    # TODO: confirm this works with multiple people
    # TODO: Why does this take so long

    if center is None:
        # if center is none, create a queryset of all centers
        center = Center.objects.all()

        if person is None:
            # if no user object was passed, go through all users for that center
            people = Person.objects.filter(center__in=center)
        else:
            # otherwise create list with just one person in it
            people = [person]
    else:
        #otherwise just use the one center
        if person is None:
            # if no user object was passed, go through all users for that center
            people = center.person_set.all()
        else:
            # otherwise create list with just one person in it
            people = [person]
#FIXME: meals not getting disabled
    for person1 in people:
        for meal in Meal.objects.filter(disabled=False):  # all():
            if not meal.disabled:
                # no need to check previously disabled meals
                avail_opts = available_options(meal, person1)
                if len(avail_opts) <= 1:
                    # if there is only one (or none) options in the list
                    # disable the meal
                    meal.disabled = True
                    meal.save()


def disable_meals2():
    """Disables meals that are past their cutoff time"""
    # TODO: investigate if the time comparison is adversely affected by timezones
    for meal in Meal.objects.all():
        cutoff_time = meal.meal_type.cutoff_time
        now = timezone.now()
        cutoff_date = meal.day.date - timezone.timedelta(days=meal.meal_type.cutoff_day)
        if cutoff_time:
            # if cutoff time is not blank combine with date to form datetime
            cutoff_datetime = timezone.timezone.combine(cutoff_date, cutoff_time)
            if cutoff_datetime < now:
                # if the cutoff datetime is earlier than now disable it
                print('disable_meals: disabled ' + str(meal))
                meal.disabled = True
                meal.save()


def increment_count(status_dict, status, person, collect_initials=False):
    """
    Increments the count in the status dict

    :param status_name:
    :param diet_name:
    :param collect_initials: if true, the counts will be list of initials
    :return:
    """
    if type(status) == Status:
        # If the parem status is of the class Status (i.e. its a model) set the report name to its name
        report_name = status.status.name
    else:
        # otherwise its a string
        report_name = status

    try:
        # Make sure that the status exists in the dict
        c_status_in_dict = status_dict[report_name]
    except KeyError:
        # if it doesnt, create it
        status_dict[report_name] = {}
        c_status_in_dict = status_dict[report_name]
        # then add all possible diets for that center
        # this is so that all diets appear on the report even if there are no counts
        # TODO: this is temporary to ease the administration into the new system
        for diet in Diet.objects.filter(center=person.center):
            if collect_initials:
                # generate empty list if initials
                c_status_in_dict[diet.name] = []
            else:
                # set == 0
                c_status_in_dict[diet.name] = 0
    try:
        # add one to the diet
        if collect_initials:
            # add initials if collecting
            c_status_in_dict[person.diet.name].append(person.initials)
        else:
            # add 1 if just counting
            c_status_in_dict[person.diet.name] += 1
    except KeyError:
        # if the diet doesn't exists make a new one
        if collect_initials:
            c_status_in_dict[person.diet.name] = [person.initials]
        else:
            c_status_in_dict[person.diet.name] = 1


def dependant_meals(meal, person, c_status, status_dict, cmeal_name, cmeal_status_n, pmeal_name, pmeal_status_n,
                    report_name, collect_initials=False):
    """
    Determines if a meal is dependant on another meal
    :param cmeal_name: Current meal name
    :param cmeal_status_n: Current meal's status name
    :param pmeal_name: Previous meal's name
    :param pmeal_status_n: Previous meal's status name
    :parem report_name: The name that will show up on the report
    :return: status_dict if counted false if not
    """

    # get the reported option (mostly for calculated options) fail if not found
    reported_option = get_object_or_404(StatusOption, name=report_name, center=person.center) #StatusOption.objects.get(name=report_name)

    # If dinner is packed and lunch is packed: packed dinner at breakfast ignore if the reported status is not active
    if meal.meal_type.name == cmeal_name and c_status.status.name == cmeal_status_n and reported_option.active:
        # if there is a packed dinner check to see if that person's lunch is also packed
        c_day = meal.day
        # get the lunch status for the day

        try:
            prev_status = person.status_set.get(meal__meal_type__name=pmeal_name, meal__day=c_day)
            if prev_status.status.name == pmeal_status_n:
                increment_count(status_dict, report_name, person, collect_initials=collect_initials)
                return True
            else:
                return False
        except Status.DoesNotExist:
            return False
    else:
        return False


def preset_status_dict(center):
    """
    returns a dict in form {option_name:{diet:count}} with all status and diets.
    NOTE: DOES NOT WORK
    :return:
    """
    status_dict = {}

    all_options = center.statusoption_set.all()
    all_diets = center.diet_set.all()

    diet_count_dict = {}
    for diet in all_diets:
        # set up dict with diet name and 0 for count. This will be used for each option
        diet_count_dict[diet.name] = 0
    for option in all_options:
        # set up status dict with option name as key and diet_count_dict as value
        status_dict[option.name] = diet_count_dict

    return status_dict


class CountObject:
    def __init__(self, status):
        """A class for making it easier to count by drawing out all the info from the status"""
        self.status = status
        self.status_opt = status.status
        self.person = status.person
        self.meal = status.meal

        self.status_n = self.status_opt.name
        self.p_status_n = self.status_opt.name  # processed status name


def status_map(CO, from_meal, from_status, check_meal_n, check_status, change_to):
    """A way to automatically change_to status to another if the current status == from_status and check_meal has check_status"""
    #TODO: develop this, make it director changeable

    # save some time by checking if check_meal_n is the same as the current meal
    if CO.meal.meal_type.name != check_meal_n:
        # get check_meal and status
        try:
            check_meal = Meal.objects.get(meal_type__name=check_meal_n, day=CO.meal.day)
            cm_status = Status.objects.get(person=CO.person, meal=check_meal)

            # If the check meal is overridden to all packed account for that
            if check_meal.override == "All Packed" and "Packed" not in cm_status.status.name and cm_status.status.name != "Absent":
                cm_status.status = StatusOption.objects.get(center=CO.person.center, name="Packed")  # change the status without saving it, so its processed below, but not saved permanently

        except Meal.MultipleObjectsReturned:
            ERROR_LOGGER.warn("tools.status_map: meal multiple objects returned for (person, meal_type_name, day): " + str(CO.person) + ', ' + check_meal_n + ', ' + str(CO.meal.day))
        except Meal.DoesNotExist:
            ERROR_LOGGER.warn("tools.status_map: status does not exist for (person, meal_type_name, day): " + str(CO.person) + ', ' + check_meal_n + ', ' + str(CO.meal.day))
            cm_status = None

    else:
        cm_status = CO.status

    # if the check meal's status and the current status are == to the parameters passed, change the current status to change_to
    if cm_status is not None:
        if cm_status.status.name == check_status and CO.p_status_n == from_status and CO.meal.meal_type.name == from_meal:
            CO.p_status_n = change_to



def count(meal_set, center, collect_initials=False, show_all=True):
    """
    Revised counting
    :param meal_set:
    :param center:
    :param collect_initials:
    :param show_all:
    :return:
    """
    center_people = center.person_set.filter(active=True)

    all_center_status_opts = StatusOption.objects.filter(center=center)

    meals = [] # [(meal, {status_option:(count, initials, diet)})]

    meal_count_dict = {}  # {meal: [COs]}
    cntr_by_meal = {} # {meal: [Counters]}

    for meal in meal_set:
        #   get all statuses for people in that center for that meal
        meal_statii = Status.objects.filter(meal=meal, person__in=center_people)

        cntr = Counter()
        initials = defaultdict(list)
        diets = defaultdict(list)

        mealCOs = []
        # loop through the statii for the meal
        # This is where the count processing happens
        for stat in meal_statii:
            CO = CountObject(stat)


            # Status Specific Overrides
            #NOTE: if an override affects a status that might affect status mapping, account for it in the status_map function

            if meal.override == "All Packed":
                # ignore meals that are already packed or absent
                if "Packed" not in CO.p_status_n and CO.p_status_n != "Absent":
                    CO.p_status_n = "Packed"


            # Special circumstances

            #  Circumstance 1: packed before breakfast vs packed at breakfast
            # make sure its an active option
            if StatusOption.objects.get(name="Packed before Breakfast", center=center).active:

                # Change packed and early packed breakfasts to packed before breakfast
                status_map(CO, 'Breakfast', 'Packed', 'Breakfast', 'Packed', 'Packed before Breakfast')
                status_map(CO, 'Breakfast', 'Early Packed', 'Breakfast', 'Early Packed', 'Packed before Breakfast')

                # Change packed lunch with a packed breakfast to packed before breakfast
                status_map(CO, 'Lunch', 'Packed', 'Breakfast', 'Packed', 'Packed before Breakfast')
                # Change packed lunch with an early packed breakfast to packed before breakfast
                status_map(CO, 'Lunch', 'Packed', 'Breakfast', 'Early Packed', 'Packed before Breakfast')
                # Change packed lunch with an early breakfast to packed before breakfast
                status_map(CO, 'Lunch', 'Packed', 'Breakfast', 'Early', 'Packed before Breakfast')
                # Change packed lunch with an absent breakfast to packed before breakfast
                status_map(CO, 'Lunch', 'Packed', 'Breakfast', 'Absent', 'Packed before Breakfast')
                # Change Early packed lunch to packed before breakfast
                status_map(CO, 'Lunch', 'Early Packed', 'Lunch', 'Early Packed', 'Packed before Breakfast')

            if StatusOption.objects.get(name="Packed at Breakfast", center=center).active:

                # Change packed lunch with a present breakfast to packed at breakfast
                status_map(CO, 'Lunch', 'Packed', 'Breakfast', 'Present', 'Packed at Breakfast')

            cntr[CO.p_status_n] += 1
            initials[CO.p_status_n].append(CO.person.initials)

            #ignore none diets
            if CO.person.diet.name != "None":
                diets[CO.p_status_n].append(CO.person.diet)

        # Manual count override
        if meal.override == "Manual Count":
            cntr['Present'] = meal.manual_count

        #Sort cntr by value: from https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
        sorted_counts = {k: v for k, v in sorted(cntr.items(), key=lambda item: item[1], reverse=True)}

        combined_dict = {} # {meal option: (count, [initials], [diets])
        for key in sorted_counts.keys(): #cntr.keys():
            combined_dict[key] = (cntr[key], initials[key], diets[key])

        meal_count_dict[meal] = combined_dict.copy() # copy to avoid issues with mutability


        meals.append((meal, combined_dict.copy()))

    return meals

def count_old(meal_set, center, collect_initials=False, show_all=True):
    """
    Possible options:
    specified by user:
        Early, Packed, Early Packed, Late, Present, Absent
    automatic:
        packed before breakfast (for lunch and dinner)
        packed at breakfast (lunch and dinner)
    :param meal_set:
    :param center:
    :param collect_initials:
    :param show_all: if true, will show all meal options, even if 0
    :return:
    """
    ignore_absent = True  # if this is true, the number of people absent will not be counted
    status_opts = center.statusoption_set.all()
    all_people = center.person_set.all()

    results_list = []  # contains [(meal,{option_name:{diet:count}})]

    # Iterate through all the meals in the set
    for meal in meal_set:

        # if show_all:
        #     # if you need to show all status options
        #     status_dict = {} #preset_status_dict(center)
        # else:
        status_dict = {}

        # then check each person's status
        for person in all_people:
            # get the person's status for that meal ignore people who dont have status

            try:
                # a = person.status_set.filter(meal=meal)
                # if len(a) > 1:
                #     print(a)
                c_status = person.status_set.get(meal=meal)

                ## All packed override ##
                if meal.override == "All Packed":
                    if c_status.status.name != "Absent" and c_status.status.name != "Packed" and c_status.status.name != "Early Packed":
                        # NOTE: This actually changes the person's status, but only for the meals displayed in the current report
                        # if the current status is not absent or already packed change it to packed
                        c_status.status = StatusOption.objects.get(name="Packed", center=center)
                        c_status.save()

                # Parems: If meal1 has status1 and meal2 has status2, add report text to status_set
                pdab_parems = [meal, person, c_status, status_dict, "Dinner", "Packed", "Lunch", "Packed",
                               "Packed at Breakfast"]
                pdbb_parems = [meal, person, c_status, status_dict, "Dinner", "Packed", "Breakfast", "Early",
                               "Packed before Breakfast"]
                pdbb1_parems = [meal, person, c_status, status_dict, "Dinner", "Packed", "Breakfast", "Early Packed",
                                "Packed before Breakfast"]
                pdbb2_parems = [meal, person, c_status, status_dict, "Dinner", "Packed", "Breakfast", "Packed",
                                "Packed before Breakfast"]
                plbb_parems = [meal, person, c_status, status_dict, "Lunch", "Packed", "Breakfast", "Early",
                               "Packed before Breakfast"]
                plbb1_parems = [meal, person, c_status, status_dict, "Lunch", "Packed", "Breakfast", "Early Packed",
                                "Packed before Breakfast"]
                plbb2_parems = [meal, person, c_status, status_dict, "Lunch", "Packed", "Breakfast", "Packed",
                                "Packed before Breakfast"]

                # NOTE: the dependant_meals func adds the option to the status_dict if true when called
                if dependant_meals(collect_initials=collect_initials, *pdbb_parems):
                    # Packed dinner before breakfast (early breakfast)
                    # Note packed dinner before breakfast has to be before packed dinner at breakfast
                    # Note this assumes that if the person is asking for an early breakfast and packed dinner,
                    #  they will need the dinner early
                    pass
                elif dependant_meals(collect_initials=collect_initials, *pdbb1_parems):
                    # Packed dinner before breakfast (early packed breakfast)
                    pass
                elif dependant_meals(collect_initials=collect_initials, *pdbb2_parems):
                    # Packed dinner before breakfast (packed breakfast)
                    pass
                elif dependant_meals(collect_initials=collect_initials, *pdab_parems):
                    # Packed dinner at breakfast
                    pass
                # If packed lunch and early or early packed breakfast: packed lunch before breakfast
                elif dependant_meals(collect_initials=collect_initials, *plbb_parems):
                    # Packed lunch before breakfast (early breakfast)
                    pass
                elif dependant_meals(collect_initials=collect_initials, *plbb1_parems):
                    # Packed lunch before breakfast (early packed breakfast)
                    pass
                elif dependant_meals(collect_initials=collect_initials, *plbb2_parems):
                    # Packed lunch before breakfast (packed breakfast)
                    pass
                else:
                    # add one to the proper key in the dictionary
                    # the key is a combination of the status' name and the diet name
                    # NOTE: Uncomment lines below to ignore Absent status
                    # if ignore_absent and c_status.status.name == 'Absent':
                    #     # do nothing
                    #     pass
                    # else:
                    increment_count(status_dict, c_status, person, collect_initials=collect_initials)

            except Status.DoesNotExist:
                pass

        ## Manual Count Override ##
        # This is after the normal count so that it only modifies the normal present count
        if meal.override == "Manual Count":
            try:
                if collect_initials:
                    status_dict['Present']["None"] = ["Manual: " + str(meal.manual_count)]
                else:
                    status_dict['Present']["None"] = meal.manual_count
            except KeyError:
                # If key error create the objects
                if collect_initials:
                    status_dict['Present'] = {"None": ["Manual: " + str(meal.manual_count)]}
                else:
                    status_dict['Present'] = {"None": meal.manual_count}

        PAB_enabled = get_object_or_404(StatusOption, name="Packed at Breakfast", center=person.center).active
        PBB_enabled = get_object_or_404(StatusOption, name="Packed before Breakfast", center=person.center).active

        # Changes "Early Packed" option for dinner to "Packed at Breakfast" if calculated option is enabled
        if meal.meal_type.name == "Dinner" and "Early Packed" in status_dict and PAB_enabled:
            if "Packed at Breakfast" in status_dict:
                # Merge the two dicts
                if collect_initials:
                    # can't use counters for initials list will produce error
                    for key in status_dict["Early Packed"].keys():
                        status_dict["Packed at Breakfast"][key] = status_dict["Packed at Breakfast"][key] + status_dict["Early Packed"][key]
                    # status_dict["Packed at Breakfast"] = status_dict["Packed at Breakfast"] + status_dict["Early Packed"]
                else:
                    status_dict["Packed at Breakfast"] = Counter(status_dict["Packed at Breakfast"]) + Counter(status_dict["Early Packed"])
                del status_dict["Early Packed"]  # remove EP entry
                # Since the counter removes 0'd diets, re-add them
                for diet in Diet.objects.filter(center=center):
                    if diet.name not in status_dict["Packed at Breakfast"]:
                        # if not in the dict set it to zero
                        status_dict["Packed at Breakfast"][diet.name] = 0
            else:
                status_dict["Packed at Breakfast"] = status_dict["Early Packed"]
                del status_dict["Early Packed"]

        # Changes "Early Packed" option for lunch to "Packed before Breakfast"
        if meal.meal_type.name == "Lunch" and "Early Packed" in status_dict and PBB_enabled:
            if "Packed before Breakfast" in status_dict:
                # Merge the two dicts
                if collect_initials:
                    # can't use counters for initials list will produce error
                    for key in status_dict["Early Packed"].keys():
                        status_dict["Packed before Breakfast"][key] = status_dict["Packed before Breakfast"][key] + \
                                                                  status_dict["Early Packed"][key]
                    # status_dict["Packed at Breakfast"] = status_dict["Packed at Breakfast"] + status_dict["Early Packed"]
                else:
                    status_dict["Packed before Breakfast"] = Counter(status_dict["Packed before Breakfast"]) + Counter(
                        status_dict["Early Packed"])
                del status_dict["Early Packed"]  # remove EP entry
                # Since the counter removes 0'd diets, re-add them
                for diet in Diet.objects.filter(center=center):
                    if diet.name not in status_dict["Packed before Breakfast"]:
                        # if not in the dict set it to zero
                        status_dict["Packed before Breakfast"][diet.name] = 0
            else:
                status_dict["Packed before Breakfast"] = status_dict["Early Packed"]
                del status_dict["Early Packed"]

        results_list.append((meal, status_dict))

    return results_list


def initials(meal_set, center):
    """
    Generates the list of initials per meal and their statuses. Basically just refers it to the count function
    with count_initials set to true
    :param meal_set:
    :param center:
    :return:
    """

    results_list = count(meal_set, center, collect_initials=True)

    return results_list


# def count2(meal_set, center):
#     """Returns the results count list for the given meal_set. meal_set is a list of meals"""
#
#     status_opts = center.statusoption_set.all()
#
#     results_list = []  # contains [(meal,{option_name:count})]
#
#     # iterate through meals, adding each option name and corresponding count to a dict for each meal
#     for meal in meal_set:
#         results_dict = {}  # dictionary that contains option_name:count
#         for opt in status_opts:
#             # Special circumstances calculations go here
#             # Note:
#             #  if there are any special calculations that use a hardcoded status option that option needs to have
#             #  can_edit set to False
#
#             if center.name == 'Schuyler':
#                 results_dict['Packed Dinner at Breakfast'] = 0
#
#                 if opt.name != 'Absent':
#                     #TODO: special cases
#                     results_dict[opt.name] = meal.status_set.filter(status=opt).count()
#                 if opt.name == 'Packed':
#                     if meal.meal_type.name == 'Dinner':
#                         # determine if packed dinner needs to be at lunch time
#                         lunch = Meal.objects.get(day=meal.day, meal_type__name='Lunch')
#                         all_packed_dinners = meal.status_set.all()
#                         a = all_packed_dinners[0]
#                         all_packed_lunches = lunch.status_set.all()
#                         early_packed_dinners = []
#                         # loop through all packed dinner statuses and lunch statuses
#                         for dinner in all_packed_dinners:
#                             for lunch in all_packed_lunches:
#                                 # if a dinner and lunch have the same person, add it to the ep dinners list
#                                 if dinner.person == lunch.person:
#                                     early_packed_dinners.append(dinner)
#                                     if dinner.person.diet.name != "None":
#                                         #TODO: working on getting diets
#                                         pass
#                         # Add the count of the ep dinners list to the results_dict
#                         results_dict['Packed Dinner at Breakfast'] = len(early_packed_dinners)
#
#
#                         #TODO: get lunch status for person and check if it is a packed (for early)
#                         # lunch_status = lunch.status_set.get(person=)
#                         # if lunch_status
#
#
#             else:
#                 if opt.name != 'Absent':
#                     # ignore absent results
#                     results_dict[opt.name] = meal.status_set.filter(status=opt).count()  # add opt_name:count to dict
#
#
#         results_list.append((meal, results_dict))
#
#     return results_list


def generate_dates():
    """
    Generates the dates up to the lookahead number.
    :return:
    """
    look_ahead = 30  # number of days ahead that should be generated
    today = timezone.now().date()
    for center in Center.objects.all():  # cycle through all centers
        try:
            latest_date = center.day_set.latest('date')  # get latest date that the center has
            date_delta = latest_date.date - today
            days_ahead = date_delta.days
        except ObjectDoesNotExist:
            latest_date = today
            days_ahead = 0

        if days_ahead < look_ahead:
            # if there are fewer than 30 days in the queue, generate new days
            if days_ahead < 0:
                # only generate days in the future
                days_ahead = 0  # reset days ahead and use it as a counter
                latest_date = today

            while days_ahead <= look_ahead:
                new_date = today + timezone.timedelta(days=days_ahead) + timezone.timedelta(days=1)
                days_ahead += 1  # increment days_ahead to keep track of new date

                # generate new Day instance
                Day(center=center, date=new_date).save()

                # the meal and status generation is taken care of in the Day.save() method


def make_json(object):
    """
    turns object into json format
    :param object:
    :return:
    """

    return json.dumps(object)

def init_actual_status(status_set):
    """
    Sets the actual status to the initial status for all items in the set.
    :param status: a Status model queryset
    :return:
    """
    status_set = status_set.filter(actual_status__isnull=True)  # eliminate all non-null items
    for status in status_set:
        # set act_status to user status and save
        status.actual_status = status.status
        status.save()