from django.contrib import admin
from django.contrib.admin import DateFieldListFilter

from .models import Person, Meal, Day, Center, Diet, Status, StatusOption, MealOption, DisableOption


def deactivate_user(modelAdmin, request, queryset):
    """
    Deactivates selected people.
    :param modelAdmin:
    :param request:
    :param queryset:
    :return:
    """
    for person in queryset:
        person.active = False
        #if ' - NA' not in person.name:
            # prevent - na from being duplicated
            #person.name += " - NA"  # this indicates on the admin page that the person is not active
        person.save()

def activate_user(modelAdmin, request, queryset):
    """
    Activates the selected people
    :param modelAdmin:
    :param request:
    :param queryset:
    :return:
    """
    for person in queryset:
        person.active = True
        #person.name = person.name.replace(' - NA', '')
        person.save()

deactivate_user.short_description = 'Deactivate Selected Persons'
activate_user.short_description = 'Activate Selected Persons'

#Admin classes
#actions: actions to apply to that user
#list_filter: list of model variables to include in the filter column
#search_fields: list of model fields to include in a search


class PersonAdmin(admin.ModelAdmin):
    actions = [deactivate_user, activate_user]


    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(PersonAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter = ['center']
            return qs
        else:
            return qs.filter(center=request.user.person.center)


class MealOptionAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(MealOptionAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter = ['center']
            return qs
        else:
            return qs.filter(center=request.user.person.center)


class DayAdmin(admin.ModelAdmin):
    search_fields = ['date']

    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(DayAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter = ['center']
            return qs
        else:
            return qs.filter(center=request.user.person.center)


class DietAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(DietAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter = ['center']
            return qs
        else:
            return qs.filter(center=request.user.person.center)


class MealAdmin(admin.ModelAdmin):
    list_filter = [('day__date', DateFieldListFilter)]
    search_fields = ['meal_type__name', 'day__date']

    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(MealAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter.append('meal_type__center')
            return qs
        else:
            return qs.filter(meal_type__center=request.user.person.center)


class StatusAdmin(admin.ModelAdmin):
    search_fields = ['person__name', 'meal__meal_type__name', 'meal__day__date']

    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(StatusAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter = ['status__center', 'person']
            return qs
        else:
            return qs.filter(status__center=request.user.person.center)


class StatusOptionAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(StatusOptionAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter = ['center']
            return qs
        else:
            return qs.filter(center=request.user.person.center)

class DisableOptionAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(DisableOptionAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            self.list_filter = ['center']
            return qs
        else:
            return qs.filter(center=request.user.person.center)


class CenterAdmin(admin.ModelAdmin):
    #list_filter = ['name', 'directors_email', 'timezone', 'show_count_in_initials']
    #search_fields = ['person__name', 'meal__meal_type__name', 'meal__day__date']
    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        qs = super(CenterAdmin, self).get_queryset(request)
        user = request.user
        # Filter by user's center, unless user is me
        if user.username == 'agiuntini':
            return qs
        else:
            return qs.filter(name=request.user.person.center.name)

admin.site.register(Person, PersonAdmin)
admin.site.register(Day, DayAdmin)
admin.site.register(Meal, MealAdmin)
admin.site.register(Center, CenterAdmin)
admin.site.register(Diet, DietAdmin)
admin.site.register(Status, StatusAdmin)
admin.site.register(StatusOption, StatusOptionAdmin)
admin.site.register(MealOption, MealOptionAdmin)
admin.site.register(DisableOption, MealOptionAdmin)
