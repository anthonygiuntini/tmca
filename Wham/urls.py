"""Wham URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.conf.urls import url

from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import ugettext_lazy as _

from meal_count import views

urlpatterns = [
    #path('', include('meal_count.urls')),
    path('admin/', admin.site.urls),
    path('redirect_to_lang_url/', views.redirect_2_lang_url)
    #url(r'^i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += i18n_patterns(
    url(r'^', include('meal_count.urls', namespace='meal-count')),
)

handler404 = 'meal_count.views.handler404'
handler403 = 'meal_count.views.handler403'
handler500 = 'meal_count.views.handler500'

# for Static Files
urlpatterns += staticfiles_urlpatterns()