###File containing preset constants and settings###
###common across both deploy and develop###
import pytz, os
import logging
import logging.config

from Wham.settings import *
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import pgettext

LOOKAHEAD = 12  # num of meals to include in the reports

AVAILABLE_OVERRIDES = ['No Override', 'All Packed', 'Manual Count']  # list of available override names
#so it will show up in the po file
TRANSOV = [_('No Override'), _('All Packed'), _('Manual Count')]

# Meal codes for translation
TRANSCODES = [pgettext('meal', 'B'), pgettext('meal', 'L'), pgettext('meal', 'D')]

VERIFY_OPTIONS = ['Absent', 'Present', 'Late', 'Packed', 'Early', 'Early Packed', '1PM']  # temporary will be an option for the directors

"""
Override Description
All Packed - Converts all non-absent statuses to "Packed"
Manual Count - Ignores all "Present" statuses for "None" diets and sets the "None-Present" count to the manual count"
-Keeps diets and packed counts the same
"""

DEFAULT_TZ = 'America/New_York'  # default tz string
DEF_TZ_I = pytz.timezone(DEFAULT_TZ)  # default NY timezone instance


# The date that is used as the starting point for the default week (must be sunday)
DEFAULT_YEAR = 2002
DEFAULT_MONTH = 10
DEFAULT_DAY = 6

DB_BACKUP_DIR = os.path.join(DATA_DIR, 'db_backups')
SAVED_REPORTS = os.path.join(DATA_DIR, 'reports')


# Logging settings

LOG_DIR = os.path.join(DATA_DIR, 'logs')
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

LOGGING = {
'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {asctime} -- {message}',
            'style': '{',
        },
    },
    'handlers': {
        'django_log': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'django_log.txt'),
            'formatter': 'simple',
        },
        'routine_log': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'routine_log.txt'),
            'formatter': 'simple',
        },
        'checkup_log': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'checkup_log.txt'),
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['django_log'],
            'level': 'INFO',
            'propagate': True,
        },
        'routine': {
            'handlers': ['routine_log'],
            'level': 'INFO',
            'propagate': True,
        },
        'checkup': {
            'handlers': ['checkup_log'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

logging.config.dictConfig(LOGGING)

ROUTINE_LOGGER = logging.getLogger('routine')
ERROR_LOGGER = logging.getLogger('django_log')
CHECKUP_LOGGER = logging.getLogger('checkup_log')
