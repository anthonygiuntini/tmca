#!/usr/bin/env python
import os
import sys

if __name__ == '__main__':
    #FIXME: this doesnt work with the terminal commands locally, since this returns false
    dev_server = (len(sys.argv) > 1 and sys.argv[1] == 'runserver')
    if dev_server:
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Wham.dev_settings')
    else:
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Wham.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
